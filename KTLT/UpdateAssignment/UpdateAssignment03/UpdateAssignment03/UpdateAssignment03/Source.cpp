﻿//UpdateAssignment3_1
/*Viết hàm nhập vào ba số, tính tích của ba số và xuất kết quả*/

/*
#include<iostream>
#include<cmath>


#define MAX 10000
using namespace std;

void NhapMang(int a[], int &n);
void TinhTich(int a[], int n);

int main()
{
	int a[MAX];
	int n;
	NhapMang(a, n);
	TinhTich(a, n);
	system("pause");
}

void NhapMang(int a[], int &n)
{
	cout << "Nhap vao so luong phan tu ma ban muon tinh tich:";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Nhap gia tri cua phan tu " << i+1 << " la:";
		cin >> a[i];
	}
}

void TinhTich(int a[], int n)
{
	int s = 1;
	for (int i = 0; i < n; i++)
	{
		s *= a[i];
	}
	cout << "Tich la:" << s << "\n";
}
*/

//UpdateAssignment3_3
/*Viết hàm nhập vào N, N < 100, tính tích từ 1 đến N, xuất kết quả*/

/*#include<iostream>
#include<cmath>
using namespace std;

int Input(int &in);
int DieuKien(int in);
int TinhTich(int in);

int main()
{
	int in,k;
	Input(in);
	k=DieuKien(in);
	TinhTich(k);
	system("pause");
}
//Hàm nhập
int Input(int &in)
{
	cout << "Chi duoc phep nhap n trong dieu kien 0 < n < 100 \n\n";
	cout << "Nhap n:";
	cin >> in;
	return in;
}
//Hàm điều kiện 0<N<100 
int DieuKien( int in)
{
	if (in <= 0 || in > 100)
	{
		do
		{
			cout << "Xem lai dieu kien (0 < n < 100) \n ";
			cout << "Moi ban nhap lai n:";
			cin >> in;
		} while (in <= 0 || in >= 100);
	}
	return in;
}
//Hàm tính tích từ 0 tới N
int TinhTich(int in)
{
	 long long int T = 1;
	for (int i = 1; i <= in; i++)
	{
		T *= i;
	}
	cout << "Tich la:" << T << "\n";
	return T;
}
*/


//UpdateAssignment3_4
/*Viết hàm nhập vào N, N < 100, tính tổng các số chẳn từ 0 đến N*/
/*
#include<iostream>
#include<cmath>
using namespace std;

int Input(int &in);
int DieuKien(int in);
int TinhTong(int in);


int main()
{
	int in, k;
	Input(in);
	k = DieuKien(in);
	TinhTong(k);
	system("pause");
}
//Hàm nhập
int Input(int &in)
{
	cout << "Chi duoc phep nhap n trong dieu kien 0 < n < 100 \n";
	cout << "Nhap n: ";
	cin >> in;
	cout << "\n";
	return in;
}
//Hàm điều kiện 0<N<100
int DieuKien(int in)
{
	if (in < 0 || in>100)
	{
		do
		{
			cout << "Xem lai dieu kien \n";
			cout << "Moi ban nhap lai n: ";
			cin >> in;
			cout << "\n";
		} while (in < 0 || in>100);
	}
	return in;
}
//Hàm tính tổng các số chắn từ 0 tới N
int TinhTong(int in)
{
	int T = 0;
	for (int i = 0; i < in; i++)
	{
		if (i % 2 == 0)
			T += i;
	}
	cout << "Tong cac so chan la:" << T << "\n";
	return T;
}
*/


//UpdateAssignment3_5
/*Viết hàm nhập vào N, N < 100, tính tổng các số chia hết cho 5 từ 0 đến N*/

/*
#include<iostream>
#include<cmath>

using namespace std;

int Input(int &in);
int DieuKien(int in);
int Tong5(int in);

int main()
{
	int in, k;
	Input(in);
	k = DieuKien(in);
	Tong5(k);
	system("pause");
}

int Input(int &in)
{
	cout<< "Chi duoc phep nhap n trong dieu kien (0 < n < 100) \n";
	cout << "Nhap n: ";
	cin >> in;
	cout << "\n";
	return in;
}

int DieuKien(int in)
{
	if (in < 0 || in>100)
	{
		do
		{
			cout << "Xem lai dieu kien\n";
			cout << "Moi ban nhap lai n: ";
			cin >> in;
			cout << "\n";
		} while (in < 0 || in>100);
	}
	return in;
}

int Tong5(int in)
{
	int T = 0;
	for (int i = 0; i <= in; i++)
	{
		if (i % 5 == 0)
			T += i;
	}
	cout << "Tong cac so chia het cho 5 tu 0 toi N la:" << T << "\n";
	return T;
}
*/

//UpdateAssignment3_6
/*Nhập vào 1 ký tự trong các ký tự A, B, C, D, E. Xuất ra ký tự in thường của ký tự
nhập vào*/
/*
#include<iostream>
#include<cmath>
#include<ctype.h>

using namespace std;
char Input(char &ca);
char DieuKien(char ca);
char Towupper(char ca);

int main()
{
	char ca, k;
	Input(ca);
	k=DieuKien(ca);
	Towupper(k);
	system("pause");
}
//Hàm nhập
char Input(char &ca)
{
	cout << "Nhap vao 1 ki tu trong cac ki tu A,B,C,D,E \n";
	cout << "Nhap vao ki tu:";
		cin >> ca;
		cout << "\n";
		return ca;
}
//Hàm điều kiện
char DieuKien(char ca)
{
	if (ca<'A' || ca>'E')
	{
		do
		{
			cout << "Xem lai dieu kien: \n";
			cout << "Moi ban nhap lai ki tu:";
			cin >> ca;
			cout << "\n";
		} while (ca<'A' || ca>'E');
	}
	return ca;
}
//Hàm chuyển kí tự hoa thành kí tự thường
char Towupper(char ca)
{
	char k;
	k = towlower(ca);
	cout << "Ki tu thuong la:" << k;
	cout << "\n";
	return k;
}
*/

//UpdateAssignment3_7
/*Nhập vào 2 ký tự trong các ký tự A, B, C, D, E. Tính tổng mã ASCII của hai ký tự
nhập vào, xuất kết quả*/
/*
#include<iostream>
#include<cmath>
#include<ctype.h>

using namespace std;
void Inputa(char &a);
void Inputb(char &b);


int main()
{
	char a, b;
	Inputa(a);
	Inputb(b);
	int c = int(a) + int(b);
	cout << "AscII: " << int(a) << "+" << int(b) << "=" << int(c) << endl;
	system("pause");
}
//Hàm nhập
void Inputa(char &a)
{
	cout << "Nhap vao 1 ki tu trong cac ki tu A,B,C,D,E \n";
	cout << "Nhap vao ki tu thu nhat: ";
	cin >> a;
	cout << endl;
	if (a<'A'||a>'E')
	{
		do
		{
			cout << "Xem lai dieu kien \n";
			cout << "Moi ban nhap lai ki tu thu nhat: ";
			cin >> a;
			cout << "\n";
		} while (a<'A' || a>'E');
	}
	cout << "Ma ASCII cua ki tu thu nhat la:" << int(a) << endl << endl;
}
//Hàm nhập
void Inputb(char &b)
{
	cout << "Nhap vao ki tu thu hai: ";
	cin >> b;
	cout << endl;
	if (b<'A' || b>'E')
	{
		do
		{
			cout << "Xem lai dieu kien \n";
			cout << "Moi ban nhap lai ki tu thu hai: ";
			cin >> b;
			cout << "\n";
		} while (b<'A' || b>'E');
	}
	cout <<"Ma ASCII cua ki tu thu hai la:"<< int(b) << endl << endl;
}
*/