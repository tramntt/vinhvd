﻿//UpdateAssignment4_1
/*Viết hàm nhập vào số, xuất ra số lớn nhất trong hai số*/
/*
#include<iostream>
#include<cmath>

using namespace std;

void Input(float &fa, float &fb);
void Max2(float fa, float fb);

int main()
{
	float fa, fb;
	Input(fa, fb);
	Max2(fa, fb);
	system("pause");
}
//Hàm nhập
void Input(float &fa, float &fb)
{
	cout << "Nhap vao so a:";
	cin >> fa;

	cout << "Nhap vao so b:";
	cin >> fb;
}
//Hàm so sánh 2 số
void Max2(float fa, float fb)
{
	float Max = fa > fb ? fa : fb;
	cout << "So lon nhat la:" << Max <<endl;
}
*/

//UpdateAssignment4_2
/*Viết hàm nhập vào n số, xuất ra số lớn nhất trong n số*/
/*
#include<iostream>
#include<cmath>

#define MAX 100

using namespace std;

void NhapMang(int a[], int &n);
void SoLonNhat(int a[], int n);


int main()
{
	int a[MAX], n;
	NhapMang(a, n);
	SoLonNhat(a, n);
	system("pause");
}
//Hàm nhập
void NhapMang(int a[], int &n)
{
	cout << "Nhap so luong phan tu n:";
	cin >> n;

	for (int i = 1; i <= n; i++)
	{
		cout << "Nhap gia tri a[" << i << "]cua tung phan tu:";
		cin >> a[i];
	}
}
//Hàm tìm số lớn nhất
void SoLonNhat(int a[], int n)
{
	int max = a[1];
	for (int i = 1; i <= n; i++)
	{
		if (a[i] > max)
			max = a[i];
	}
	cout << "So lon nhat la:" << max << endl;
}
*/

