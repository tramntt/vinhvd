﻿//UpdateAssignment5_1
/*Viết hàm nhập vào N, tính kết quả dãy số: 1/2+2/3+3/4+...+(n-1)/n, xuất kết quả*/
/*
#include<iostream>
#include<cmath>

void InputN(float &fn);
float Ass5_2(float fn);

using namespace std;

int main()
{
	float fn;
	InputN(fn);
	Ass5_2(fn);
	system("pause");
}

//Hàm nhập
void InputN(float &fn)
{
	cout << "Nhap N: ";
	cin >> fn;
}

//Viết hàm nhập vào N, tính kết quả dãy số: 1/2+2/3+3/4+...+(n-1)/n, xuất kết quả
float Ass5_2(float fn)
{
	float S = 0;
	for (float i = 2; i <= fn; i++)
	{
		S += (i-1)/i;
	}
	cout << "In ra ket qua:" << S << endl;
	return S;
}
*/
//UpdateAssignment5_7
/*Viết hàm nhập vào N, tính kết quả dãy số:(1*2)+(3*4)+(4*5)+...+((n-1)*n)*/
/*
#include<iostream>
#include<cmath>
using namespace std;

void Input(float &in);
float Ass_7(float in);

int main()
{
	float in;
	Input(in);
	Ass_7(in);
	system("pause");
}
//Hàm nhập N
void Input(float &in)
{
	cout << "Nhap vao n:";
	cin >> in;
}
//Hàm tính dãy số
float Ass_7(float in)
{
	float S = 0;
	for (int i = 1; i <= in; i++)
	{
		S += ((i - 1)*i);
	}
	cout << "In ra ket qua:" << S << endl;
	return S;
}
*/


