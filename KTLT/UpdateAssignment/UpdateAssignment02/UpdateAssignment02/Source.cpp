﻿//UpdateAssignment2_4
/*Viết hàm nhập vào hai số, tính số dư của phép chia hai số và xuất kết quả*/

#include<iostream>
#include<cmath>

void Input(int &fa, int &fb);
int Sodu(int fa, int fb);

using namespace std;

int main()
{
	int fa, fb;

	Input(fa, fb);
	Sodu(fa, fb);

	system("pause");
}

//Hàm nhập
void Input(int &fa, int &fb)
{
	cout << "a/b" << "\n";
	cout << "Nhap vao so a:";
	cin >> fa;

	cout << "Nhap vao so b:";
	cin >> fb;
}

//Hàm tính toán
int Sodu(int fa, int fb)
{
	int fc = fa % fb;
	if (fa%fb == 0)
		cout << "Khong co so du \n";
	else
		cout << "So du cua phep chia " << fa << "/" << fb << " la: " << fc << "\n";
	return fc;
}

//UpdateAssignment2_5
/*Viết hàm nhập vào hai số, xuất ra kết quả “TONG CHAN” nếu tổng là chẳn, “TONG
LE" nếu tổng là lẻ*/

#include<iostream>
#include<math.h>

void Input(int &fa, int &fb);
void TongChanLe(int fa, int fb);

using namespace std;

int main()
{
	int fa, fb;
	Input(fa, fb);
	TongChanLe(fa, fb);
	system("pause");
}

//Hàm nhập
void Input(int &fa, int &fb)
{
	cout << "a/b" << "\n";
	cout << "Nhap vao so a:";
	cin >> fa;

	cout << "Nhap vao so b:";
	cin >> fb;
}


//Viết hàm nhập vào hai số, xuất ra kết quả "TONG CHAN" nếu tổng là chẵn, "TONG LE" nếu tổng là lẻ
void TongChanLe(int fa, int fb)
{
	int fc = fa + fb;
	cout << "Ket qua la: " << fc << "\n";
	if (fc % 2 == 0)
		cout << "TONG CHAN \n";
	else
		cout << "TONG LE \n";
}