//Assignment10_1_1
//T�nh S(n) = 1 + 2 + 3 + ... + n
/*
#include<iostream>
#include<cmath>

using namespace std;

void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int &n)
{
	int s = 0;
	for (int i = 0; i <= n; i++)
	{
		s += i;
	}
	cout << "Tong la:" << s << endl;
}
*/

//Assignment10_1_2
//T�nh S(n) = 1^2 + 2^2 + ... + n^2
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &n);
void HamXuLi(int &n);


int main()
{
	int n;
	Input(n);
	HamXuLi(n);//?????
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int &n)
{
	int s = 0;
	for (int i = 0; i <= n; i++)
	{
		s += pow(i, 2);
	}
	cout << "Ket qua :" << s << endl;
}
*/

//Assignment10_1_3
//T�nh S(n) = 1 + � + 1/3 + ... + 1/n
/*
#include<iostream>
#include<cmath>

using namespace std;

void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int &n)
{
	float s = 0;
	for (float i = 1; i <= n; i++)//float i (ko dc int i)
	{
		s = s + (1/i);
	}
	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_1_4
//T�nh S(n) = � + � + ... + 1/2n
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Dieu kien n la so chan (#0) va n khong la so am" << endl;
	cout << "Nhap n:";
	cin >> n;
	cout << endl;

	if (n < 0 || n % 2 != 0)
	{
		do
		{
			cout << "Moi ban xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n < 0 || n % 2 != 0);
	}
}

void HamXuLi(int &n)
{
	float s = 0;
	for (float i = 2; i <= n; i+=2)
	{
		s += (1 / (i));
	}
	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_1_5
//T�nh S(n) = 1 + 1/3 + 1/5 + ... + 1/(2n + 1)
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Dieu kien n la so le va n khong la so am" << endl;
	cout << "Nhap n:";
	cin >> n;
	cout << endl;

	if (n <= 0 || n % 2 == 0)
	{
		do
		{
			cout << "Moi ban xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n <= 0 || n % 2 == 0);
	}
}

void HamXuLi(int &n)
{
	float s = 0;
	for (float i = 1; i <= n; i+=2)
	{
		s += (1 / i);
	}
	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_1_6
//T�nh S(n) = 1/(1x2) + 1/(2x3) +...+ 1/n x (n + 1)
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int &n)
{
	float s = 0;
	for (float i = 1; i <= n; i++)
	{
		s += (1 / (i * (i + 1)));
	}
	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_1_7
//T�nh S(n) = � + 2/3 + � + .... + n / n + 1
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap n";
	cin >> n;
}

void HamXuLi(int &n)
{
	float s = 0;
	for (float i = 1; i <= n; i++)
	{
		s += (i / (i + 1));//can than ngoac
	}
	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_1_8
//T�nh S(n) = � + � + 5/6 + ... + 2n + 1/ 2n + 2
/*
#include<iostream>
#include<cmath>

using namespace std;

void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int &n)
{
	float s = 0;
	for (float i = 0; i <= n; i++)
	{
		s += (((2 * i) + 1) / ((2 * i) + 2));
	}

	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_1_9
//T�nh T(n) = 1 x 2 x 3...x N
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &n);
void HamXuLi(int &n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int &n)
{
	int s = 1;
	for (int i = 1; i <= n; i++)
	{
		s *= i;
	}
	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_1_10
//T�nh T(x, n) = x^n
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &x, int &n);
void HamXuLi(int &x, int &n);

int main()
{
	int x, n;
	Input(x, n);
	HamXuLi(x, n);
	system("pause");
}

void Input(int &x, int &n)
{
	cout << "Nhap x: ";
	cin >> x;

	cout << "Nhap n: ";
	cin >> n;
}

void HamXuLi(int &x, int &n)
{
	int s = 1;
	for (int i = 1; i <= n; i++)
	{
		s *= x;
	}
	cout << "Ket qua la:" << s << endl;
}
*/