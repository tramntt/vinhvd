//Assignment10_1
//T�nh S(n) = 1 + 1.2 + 1.2.3 + ... + 1.2.3....N
/*
#include<iostream>

using namespace std;

void Input(int &n);
void HamXuli(int n);

int main()
{
	int n;
	Input(n);
	HamXuli(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap vao n:";
	cin >> n;
}

void HamXuli(int n)
{
	int s = 0;
	int k = 1;
	for (int i = 1; i <= n; i++)
	{
		k = k * i;
		s = s + k;
	}
	cout << "Tong la:" << s << endl;
}
*/
//Assignment10_2
//T�nh S(n) = x + x^2 + x^3 + ... + x^n
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &x, int &n);
void HamXuLi(int x, int n);

int main()
{
	int x, n;
	Input(x, n);
	HamXuLi(x, n);
	system("pause");
}

void Input(int &x, int &n)
{
	cout << "Nhap vao x:";
	cin >> x;

	cout << "Nhao vao n:";
	cin >> n;
}

void HamXuLi(int x, int n)
{
	int s = 0;
	for (int i = 1; i <= n; i++)
	{
		s = s + pow(x, i);//n
	}
	cout << "Xuat ra tong:" << s << endl;
}
*/

//Assignment10_3
//T�nh S(n) = x^2 + x^4 + ... + x^2n
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &x, int &n);
void HamXuLi(int x, int n);

int main()
{
	int x, n;
	Input(x, n);
	HamXuLi(x, n);
	system("pause");
}

void Input(int &x, int &n)
{

	cout << "Nhap vao x:";
	cin >> x;
	cout << endl;

	cout << "Dieu kien n la so chan va ko am" << endl;
	cout << "Moi ban nhap n:";
	cin >> n;
	cout << endl;
	if (n <= 0 || n % 2 != 0)
	{
		do
		{
			cout << "Moi ban xem lai dieu kien" << endl;
			cout << "Nhap vao n:";
			cin >> n;
			cout << endl;
		} while (n <= 0 || n % 2 != 0);
	}
}

void HamXuLi(int x, int n)
{
	int s = 0;
	for (int i = 2; i <= n; i +=2)//i=+2
	{
		s = s + pow(x, i);
	}
	cout << "Tong la:" << s << endl;
}
*/

//Assignment10_4
//T�nh S(n) = x + x^3 + x^5 + ... + x^2n + 1
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &x, int &n);
void HamXuLi(int x, int n);

int main()
{
	int x, n;
	Input(x, n);
	HamXuLi(x, n);
	system("pause");
}

void Input(int &x, int &n)
{
	cout << "Nhap vao x:";
	cin >> x;
	cout << endl;

	cout << "Dieu kien n la so le va n khong am:" << endl;
	cout << "Moi ban nhap n: ";
	cin >> n;

	if (n <= 0 || n % 2 == 0)
	{
		do 
		{
			cout << "Moi ban xem lai dieu kien " << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n <= 0 || n % 2 == 0);
	}
}

void HamXuLi(int x, int n)
{
	int s = 0;
	for (int i = 1; i <= n; i += 2)
	{
		s += pow(x, i);
	}
	cout << "Xuat ra tong:" << s << endl;
}
*/

//Assignement10_5
//T�nh S(n) = 1 + 1/(1 + 2) + 1/( 1 + 2 + 3) + ..... + 1/ 1 + 2 + 3 + .... + N
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &n);
void HamXuLi(int n);

int main()
{
	int n;
	Input(n);
	HamXuLi(n);
	system("pause");
}

void Input(int &n)
{
	cout << "Nhap vao n:";
	cin >> n;
}

void HamXuLi(int n)
{
	float s = 0;
	float k = 0;
	for (int i = 1; i <= n; i++)//thay doi i=1 vs k=0 (i=0;k=1)
	{
		k = k + i;
		s = s +(1/k);
	}
	cout << "Tong la " << s << endl;
}
*/

//Assignment10_6
//T�nh S(n) = x + x^2/(1 + 2 )+ x^3/(1 + 2 + 3) + ... + x^n/1 + 2 + 3 + .... + N
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &x, int &n);
void HamXuLi(int x, int n);

int main()
{
	int x, n;
	Input(x, n);
	HamXuLi(x, n);
	system("pause");
}

void Input(int &x, int &n)
{
	cout << "Nhap x:";
	cin >> x;

	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int x, int n)
{
	float s = 0;
	float k = 0;
	for (int i = 1; i <= n; i++)
	{
		k +=i;
		s = s + (pow(x, i) / k);
	
	}
	cout << "Ket qua la:" << s << endl;
}
*/

//Assignment10_7
//T�nh S(n) = x + (x^2)/2! + (x^3)/3! + ... + x^n/N!
/*
#include<iostream>
#include<cmath>

using namespace std;
void Input(int &x, int &n);
void HamXuLi(int x, int n);

int main()
{
	int x, n;
	Input(x, n);
	HamXuLi(x, n);
	system("pause");
}

void Input(int &x, int &n)
{
	cout << "Nhap x:";
	cin >> x;

	cout << "Nhap n:";
	cin >> n;
}

void HamXuLi(int x, int n)
{
	float s = 0;
	float k = 1;
	for (int i = 1; i <= n; i++)
	{
		k *= i;
		s += (pow(x, i) / k);
	}
	cout << "Ket qua la:" << s << endl;
}
*/