﻿//Assignment12_1
//Hãy liệt kê các số âm trong mảng 1 chiều các số thực

/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;
void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
void HamLietKeSoAm(float a[], int n);

int main()
{
	float a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamLietKeSoAm(a, n);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien (0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu cua n:";
	cin >> n;
	cout << endl;
	if (n < MIN || n > MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap vao so luong phan tu n:";
			cin >> n;
		} while (n < MIN || n>MAX);
	}
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(float a[], int n)
{
	cout << "Mang so thuc ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void HamLietKeSoAm(float a[], int n)
{
	cout << "Cac so am la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] < 0)
			cout << a[i] << '\t';
	}
	cout << endl;
}
*/
//Assignment12_2
//Hãy liệt kê các số trong mảng 1 chiều các số thực thuộc đoạn [x, y] cho trước

/*
#include<iostream>
#include<cmath>
#define MAX 100
#define MIN 0
using namespace std;
void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
void HamXuatXY(float a[], int n, float x, float y);

int main()
{
	float a[MAX], x, y;
	int n;
	HamNhap(a, n);
	cout << "Nhap vao gioi han ben trai x:";
	cin >> x;

	cout << "Nhap vao gioi han ben phai y:";
	cin >> y;

	HamXuat(a, n);
	HamXuatXY(a, n,x,y);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien (0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;
	if (n < MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
		} while (n < MIN || n>MAX);
	}
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(float a[], int n)
{
	cout << "Mang so thuc ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void HamXuatXY(float a[], int n,float x, float y)
{	
	cout << "Mang thuoc doan x[...]y la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		if (x <= a[i] && a[i] <= y)
			cout << a[i] << '\t';
	}
	cout << endl;
}
*/

//Assignment12_3
/*Hãy liệt kê các số chẵn trong mảng 1 chiều các số nguyên thuộc đoạn [x, y]
cho trước(x, y là các số nguyên)*/

/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;
void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
void HamXuatSoChanTrongXY(int a[], int n, int x, int y);

int main()
{
	int a[MAX], x, y;
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	cout << "Nhap vao gioi han ben trai x:";
	cin >> x;

	cout << "Nhap vao gioi han ben phai y:";
	cin >> y;
	cout << endl;

	
	HamXuatSoChanTrongXY(a, n, x, y);
	system("pause");

}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien (0<n<100)" << endl;
	cout << "Moi ban nhap n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void HamXuatSoChanTrongXY(int a[], int n, int x, int y)
{
	cout << "Cac so chan trong mang x[...]y" << endl;
	for (int i = 1; i <= n; i++)
	{
		if ((x <= a[i]) && (a[i] <= y) && (a[i] % 2 == 0))
			cout << a[i] << '\t';
	}
	cout << endl;
}
*/

//Assignment12_4
/*Hãy liệt kê các giá trị trong mảng mà thỏa điều kiện lớn hơn giá trị tuyệt đối
của giá trị đứng liền sau nó*/

/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;
void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
void TuyetDoi(int a[], int n);

int main()
{
	int a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	TuyetDoi(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien (0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void TuyetDoi(int a[], int n)
{
	cout << "Gia tri trong mang thoa dieu kien:" << endl;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] > abs(a[i - 1]) && a[i] < abs(a[i + 1]))
			cout << a[i] << '\t';
	}
	cout << endl;
}
*/
//Assignment12_5
/*Hãy liệt kê các giá trị trong mảng mà thỏa điều kiện nhỏ hơn trị tuyệt đối
của giá trị đứng liền sau nó và lớn hơn trị tuyệt đối của giá trị đứng liền
trước nó*/

/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

int main()
{

}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien (0<n<100)";
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;
	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:";
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

*/

//Assignment12_6
//Tính tổng các phần tử trong 
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
void HamTinhTong(float a[], int n);

int main()
{
	float a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamTinhTong(a, n);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n < MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(float a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}
void HamTinhTong(float a[], int n)
{
	cout << "Tong cac phan tu trong mang la:" << endl;
	float s = 0;
	for (int i = 1; i <= n; i++)
	{
		s = s + a[i];
	}
	cout << s << endl;
}
*/

//Assignment12_7
//Tính tổng các giá trị dương trong mảng 1 chiều các số thực
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
void HamXuLi(float a[], int n);

int main()
{
	float a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamXuLi(a, n);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien (0<n<100)" << endl;
	cout << "Nhap vao so luong pha tu cua n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(float a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}
//Hàm tính tổng các giá trị dương
void HamXuLi(float a[], int n)
{
	cout << "Tong cac gia tri duong trong mang la:" << endl;
	float s = 0;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] > 0)
			s += a[i];
	}
	cout << s << endl;
}
*/

//Assignment12_8
/*Tính tổng các giá trị có chữ số đầu tiên là chữ số lẻ trong mảng 1 chiều các
số nguyên*/

/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
int HamTimChuSoDauLe(int n);
void HamXuLi(int a[], int n);

int main()
{
	int n, a[MAX];
	HamNhap(a, n);
	HamXuat(a, n);
	HamTimChuSoDauLe(n);
	HamXuLi(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien (0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

int HamTimChuSoDauLe(int n)
{
	int idv;
	n = abs(n);
	while(n>=10)
	{
		idv = n % 10;
		n = n / 10;
	}
	if (n % 2 == 0)
	{
		return 0;
	}
	return 1;
}
//Tính tổng các giá trị có chữ số đầu tiên là chữ số lẻ trong mảng 1 chiều 
void HamXuLi(int a[], int n)
{
	cout << "Tong cac gia tri co chu so dau tien la chu so le la:" << endl;
	int s = 0;
	for (int i = 1; i <= n; i++)
	{
		if (HamTimChuSoDauLe(a[i]) == 1)
		{
			s += a[i];
		}
	}
	cout << s << endl;
}
*/

//Assignment12_9 CHƯA XONG
/*Tinh tổng các chữ số có chữ số hàng chục là 5 trong mảng 1 chiều các số
nguyên*/

/*
#include <iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;
void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
int	HamTimSoHangChuc(int n);
int HamXuLi(int a[], int n);

int main()
{
	int a[MAX];
	int n, tong;
	HamNhap(a, n);
	HamXuat(a, n);
	HamTimSoHangChuc(n);
	double kq = HamXuLi(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

int HamTimSoHangChuc(int n)
{
	n = abs(n);
	n = n / 10;
	int hc = n % 10;
	if (hc == 5)
	{
		return 1;
	}
	return 0;
}

//Tinh tổng các chữ số có chữ số hàng chục là 5 trong mảng 1 chiều các số nguyên
int HamXuLi(int a[], int n)
{
	cout << "Tong cac chu so co chu so hang chuc la 5: " << endl;
	double s = 0;
	for ( int i = 1; i <= n  ; i++)
	{
		if (HamTimSoHangChuc(a[i] == 1))
		{
			s =s+ a[i];
		}
	}
	return s;
}
*/

//Asignment12_10
//Đếm số lượng số chẵn trong mảng. Đếm số dương chia hết cho 7 trong mảng

/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;
void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
void HamXuLiChan(int a[], int n);
void DemSoDuong7(int a[], int n);

int main()
{
	int a[MAX], n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamXuLiChan(a, n);
	DemSoDuong7(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Moi ban xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl << endl;
}

void HamXuLiChan(int a[], int n)
{
	cout << "So luong phan tu chan trong mang la:" ;
	int dem = 0;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] % 2 == 0)
			dem++;
	}
	cout << dem << endl;
}

void DemSoDuong7(int a[], int n)
{
	cout << "So luong phan tu chia het cho 7 trong mang la:";
	int dem = 0;
	for (int i = 1; i <= n; i++)
	{
		if ((a[i] > 0) && (a[i] % 7 == 0))
			dem++;
	}
	cout << dem << endl << endl;
}
*/

//Assignment12_11
//Đếm số lần xuất hiện của giá trị x trong mảng
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;
void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
void HamXuLi(int a[], int n);

int main()
{
	int a[MAX], n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamXuLi(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void HamXuLi(int a[], int n)
{
	int x;
	cout << "Nhap vao phan tu ma ban muon kiem tra so lan xuat hien trong mang: ";
	cin >> x;
	cout << endl;

	int k = 0;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] == x)
			k++;
	}
	cout << "So lan ma phan tu " << x << " xuat hien trong mang la: ";
	cout << k << endl;
}
*/

//Assignment12_12
//Đếm số lượng giá trị tận cùng bằng 5 trong mảng

/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;
void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
void HamXuLi(int a[], int n);

int main()
{
	int a[MAX], n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamXuLi(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu Kien (0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;
	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n:";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void HamXuLi(int a[], int n)
{
	cout << "Dem so luong gia tri tan cung bang 5 la:" << endl;
	int k = 0;
	for (int i = 1; i <= n; i++)
	{
		if ((a[i] % 5 == 0) && (a[i] % 2 != 0))	
			k++;
	}
	cout << k << endl;
}
*/





