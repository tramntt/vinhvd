﻿#include<iostream>
#include<math.h>
#include<conio.h>

void InputN(float &fn);
float Ass5_6(float fn);

int main()
{
	float fn;
	InputN(fn);
	Ass5_6(fn);
	system("pause");
}

//Hàm nhập
void InputN(float &fn)
{
	printf("Nhap n:");
	scanf_s("%f", &fn);
}

//Viết hàm nhập vào N, tính kết quả dãy số: (1*2)+(2*3)+(3*4)+...+((n-1)*n), xuất kết quả
float Ass5_7(float fn)
{
	float S = 0, k = 1;
	for (float i = 2; i <= fn; i++)
	{
		S += k * i;
		k++;
	}
	printf("In ra ket qua: %0.3f\n", S);
	return S;
}


