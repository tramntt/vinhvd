﻿//Assignment6_3
/*Khởi tạo mảng tạo mảng số nguyên là các số ngẩu nhiên từ 0 đến 100, tính tổng
các giá trị có trong mảng, xuất kết quả*/


#include<iostream>
#include<cmath>
#include<cstdlib>

#define MAX 100

using namespace std;

int main()
{
		int array[MAX];
			//Hàm nhập số ngẫu nhiên
			for (int i = 0; i < MAX; i++)
			{
				array[i] = rand() % 100 + 1;
			}
			//Hàm xuất
			for (int i = 0; i < MAX; i++)
			{
				cout << " Arr[" << i << "] =" << array[i] << "\n";
			}
			//Hàm tính tổng
			int s = 0;
			for (int i = 0; i < MAX; i++)
			{
			s = s + array[i];
			}

			cout << "Tong la: " << s << "\n";
			system("pause");
			return 0;
}

//Assignment6_4
/*Khởi tạo mảng tạo mảng số nguyên là các số ngẩu nhiên từ 0 đến 100, tính tổng
các giá trị ở vị trí lẻ trong mảng, xuất kết quả*/

#include<iostream>
#include<cmath>
#include<cstdlib>
#include<ctime>

#define MAX 1000

using namespace std;

int main()
{
	int array[MAX];
	srand(time(0));
	//Hàm nhập mảng tự động các số nguyên từ 0 tới 100
	for (int i = 0; i < MAX; i++)
	{
		array[i] = rand() % 100 + 1;
	}

	//Hàm xuất
	for (int i = 0; i < MAX; i++)
	{
		cout << " Arr[" << i << "] =" << array[i] << "\n";
	}

	//Hàm tính tổng các giá trị lẻ trong mảng
	int s = 0;
		for (int i = 0; i < MAX; i++)
		{
			if (array[i] % 2 == 1 )
				s = s + array[i];
		}

	cout << "Tong la: " << s << "\n";
	system("pause");
	return 0;
}



//Assignemnt6_1
/*Viết hàm nhập vào số N, khởi tạo mảng các giá trị từ 0 đến N, xuất kết quả giá trị
của mảng*/

#include<iostream>
#include<cstdlib>

#define MAX 1000
using namespace std;

void NhapMang(int a[], int &n);
void XuatMang(int a[], int n);

int main()
{
	int a[MAX];
	int n;
	NhapMang(a, n);
	XuatMang(a, n);
	system("pause");
}
//Hàm nhập 
void NhapMang(int a[], int &n)
{
	cout << "Nhap so luong phan tu n:";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		do
		{
			cout << "Nhap vao phan tu a[" << i << "]=";
			cin >> a[i];
		} while (a[i] > n);
	}
	cout << "\n";
}
//Hàm xuất
void XuatMang(int a[], int n)
{
	cout << "Xuat mang:";
	for (int i = 0; i < n; i++)
	{
		cout << "\t"<< a[i];
	}
	cout << "\n";
}

//Assignment6_2
/*Viết hàm nhập vào số N, khởi tạo mảng các giá trị lẻ từ 0 đến N, xuất kết quả vị trí
của các giá trị có trong mảng*/

#include<iostream>
#include<cstdlib>

#define MAX 1000
using namespace std;

void NhapMang(int a[], int &n);
void XuatMang(int a[], int n);

int main()
{
	int a[MAX];
	int n;
	NhapMang(a, n);
	XuatMang(a, n);
	system("pause");
}
//Hàm nhập 
void NhapMang(int a[], int &n)
{
	cout << "Nhap so luong phan tu n:";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		do
		{
			cout << "Nhap vao phan tu a[" << i << "]=";
			cin >> a[i];
		} while (a[i] % 2 == 0 && a[i] > n);
	}
	cout << "\n";
}
//Hàm xuất
void XuatMang(int a[], int n)
{
	cout << "Xuat mang:";
	for (int i = 0; i < n; i++)
	{
		cout << "\t" << a[i];
	}
	cout << "\n";
}

//Assignment6_5
/*Nhập vào N, khởi tạo mảng tạo mảng số nguyên là các số ngẩu nhiên từ 0 đến N,
tính tổng các giá trị > 10 trong mảng, xuất kết quả*/

#include<iostream>
#include<ctime>
#include<cstdlib>

#define MAX 100
using namespace std;
void NhapMang(int a[], int &n);
void XuatMang(int a[], int n);
void TinhTong(int a[], int n);


int main()
{
	int a[MAX];
	int n;
	NhapMang(a, n);
	XuatMang(a, n);
	TinhTong(a, n);
	system("pause");
}
//Ham Nhập 
void NhapMang(int a[], int &n)
{
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{
		a[i] = rand() % n + 0;
	}
}
//Hàm Xuất
void XuatMang(int a[], int n)
{
	for(int i=0;i<n;i++)
	{
		cout << "a[" << i << "]=" << a[i] << "\n";
	}
}
//Hàm tính tổng các giá trị lớn hơn 10 trong mảng
void TinhTong(int a[], int n)
{
	int s = 0;
	for (int i = 0; i < n; i++)
	{
		if (a[i] > 10)
			s = s + a[i];
	}
	cout << "Tong la:" << s << "\n";
}




///////////////////////////////////////////////////////////////
//#include<iostream>
//#include<cmath>
//#include<ctime>
//#include<cstdlib>
//#include<random>
//
//using namespace std;
//
//#define max 5
//
//int main()
//{
//	int array[max];// c++11
//	random_device rd;
//	mt19937_64 rng(rd());
//	uniform_int_distribution<int> uni(1, 100);
//	for (int i = 0; i < max; i++)
//	{
//		auto n = uni(rng);
//		cout << "thu tu cua " << i << " la: " << n << endl;
//	}
//	int s = 0;
//	for (int i = 0; i < max; i++)
//	{
//		s = s + uni(rng);
//	}
//	cout << "tong la: " << s << "\n";
//	system("pause");
//	return 0;
//}