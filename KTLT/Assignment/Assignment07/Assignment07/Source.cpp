﻿//Assignment7_1
/*Nhập vào N, khởi tạo mảng tạo mảng số nguyên là các số ngẫu nhiên từ 0 đến N,
tìm giá trị lớn nhất trong mảng*/

#include<iostream>
#include<cstdlib>
#include<ctime>

#define MAX 2000

using namespace std;

void NhapMang(int a[], int &n);
void XuatMang(int a[], int n);
void LonNhat(int a[], int n);


int main()
{
	int a[MAX];
	int n;

	NhapMang(a, n);
	XuatMang(a, n);
	LonNhat(a, n);
	system("pause");
	return 0;
}
//Hàm nhập
void NhapMang(int a[], int &n)
{
	srand(time_t(NULL));
	cout << "Nhap so luong phan tu n: ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		a[i] = rand() % n + 1;
	}
}
//Hàm xuất
void XuatMang(int a[], int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << "arr[" << i << "] = " << a[i] << endl;
	}
}

//Hàm tìm giá trị lớn nhất
void LonNhat(int a[], int n)
{
	int max;
	max = a[0];
	for (int i = 0; i < n; i++)
	{
		if (max < a[i])
		{
			max = a[i];
		}
	}
	cout << "So lon nhat trong mang la:" << max << "\n";
}

//Assignment7_2
/*Nhập vào N, khởi tạo mảng tạo mảng số nguyên là các số ngẫu nhiên từ 0 đến N,
tìm giá trị LẺ lớn nhất trong mảng*/

#include<iostream>
#include<cstdlib>
#include<ctime>

#define MAX 100

using namespace std;

void NhapMang(int a[], int &n);
void XuatMang(int a[], int n);
void LonNhat(int a[], int n);


int main()
{
	int a[MAX];
	int n;

	NhapMang(a, n);
	XuatMang(a, n);
	LonNhat(a, n);
	system("pause");
	return 0;
}
//Hàm nhập
void NhapMang(int a[], int &n)
{
	srand(time_t(NULL));
	cout << "Nhap so luong phan tu n: ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		a[i] = rand() % n + 1;
	}
}
//Hàm xuất
void XuatMang(int a[], int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << "arr[" << i << "] = " << a[i] << endl;
	}
}

//Hàm tìm giá trị lớn nhất
void LonNhat(int a[], int n)
{
	int max;
	max = a[0];
	for (int i = 0; i < n; i++)
	{
		if (max < a[i] && a[i] % 2==1)
		{
			max = a[i];
		}
	}
	cout << "so le lon nhat trong mang la:" << max << "\n";
}

//Assignment7_3
/*Khởi tạo mảng các số từ 1 đến 100, xuất ra kết quả các giá trị từ cuối đến đầu 
mảng*/

#include<iostream>
#include<ctime>
#include<cstdlib>

#define MAX 100

void NhapMang(int a[], int &n);
void XuatMang(int a[], int n);

using namespace std;

int main()
{
	int a[MAX];
	int n;
	NhapMang(a, n);
	XuatMang(a, n);
	system("pause");
}

void NhapMang(int a[],int &n)
{
	/*cout << "Nhap so luong phan tu n: ";
	cin >> n;*/
	int k = 100;
	for (int i = 0; i <MAX; i++)
	{
		//cout << "arr[" << i << "] = " << a[i] << endl;
		a[i] = k;
		k--;
	}
}

void XuatMang(int a[], int n)
{
	
	for (int i = 0; i < MAX; i++)
	{
		cout << "arr[" << i << "] = " << a[i] << endl;
	}
}

//Assignmnent7_4
/*Nhập vào N, N là số chẵn, khởi tạo mảng tạo mảng số nguyên là các số ngẫu
nhiên từ 0 đến N, xuất ra phần tử ở giữa mảng.*/

//#include<iostream>
//#include<cstdlib>
//#include<ctime>
//
//#define MAX 100
//
//void NhapMang(int a[], int &n);
//void XuatMang(int a[], int n);
//
//using namespace std;
//
//int main()
//{
//	int a[MAX];
//	int n;
//	NhapMang(a, n);
//	XuatMang(a, n);
//	system("pause");
//
//}
////Hàm nhập mảng
//void NhapMang(int a[], int &n)
//{
//	cout << "Dieu kien: Nhap n chan" << endl;
//		cout << "Moi ban nhap n:";
//		cin >> n;
//		if (n % 2 != 0)
//		{
//			do
//			{
//				cout << "Xem lai dieu kien " << endl;
//				cout << "Nhap so luong phan tu n:";
//				cin >> n;
//				cout << endl;
//			} while (n % 2 != 0);
//		}
//	srand(time(NULL));
//
//	for (int i = 1; i <= n; i++)
//	{
//		a[i] = rand() % n + 0;
//	}
//}
////Hàm xuất mảng
//void XuatMang(int a[], int n)
//{
//	
//	int k;
//	for (int i = 1; i <= n; i++)
//	{
//		k=i/2;
//		//cout << "phan tu o giua mang la:" << a[k];
//		cout << "arr[" << i << "] = " << a[i] << endl;
//	}
//	cout << " Phan tu arr[" << k << "] giua mang la:" << a[k] << "\n";
//}


