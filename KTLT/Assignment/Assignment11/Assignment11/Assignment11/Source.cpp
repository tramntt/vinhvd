﻿//Assignment11_1
//Viết hàm nhập, xuất mảng 1 chiều các số thực
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);

int main()
{
	float a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
		cout << endl;
	}
}

void HamXuat(float a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;

	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}
*/

//Assignment11_2
//Viết hàm nhập, xuất mảng 1 chiều các số nguyên
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);

int main()
{
	int a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{

	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
		cout << endl;
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang so nguyen ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}
*/

//Assignment11_3
//Viết hàm liệt kê các giá trị chẵn trong mảng 1 chiều các số nguyên
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
void HamXuatChan(int a[], int n);

int main()
{
	int a[MAX], n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamXuatChan(a, n);
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void HamXuatChan(int a[], int n)
{
	cout << "Cac gia tri chan trong mang so nguyen la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] % 2 == 0)
			cout << a[i] << '\t';
	}
	cout << endl;
}
*/

//Assignment11_4
//Viết hàm liệt kê các vị trí mà giá trị tại đó là giá trị âm trong mảng 1 chiều các số thực
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
void HamXuatSoThucAm(float a[], int n);


int main()
{
	float a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamXuatSoThucAm(a, n);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
		cout << endl;
	}
	cout << endl;
}

void HamXuat(float a[], int n)
{
	cout << "Mang so nguyen ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

void HamXuatSoThucAm(float a[], int n)
{
	cout << "Mang so thuc am la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] < 0)
			cout << a[i] << '\t';
	}
	cout << endl;
}
*/

//Assignment11_5
//Viết hàm tìm giá trị lớn nhất trong mảng 1 chiều các số thực
/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
void HamXuatSoLonNhat(float a[], int n);

int main()
{
	float a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	HamXuatSoLonNhat(a, n);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
		cout << endl;
	}
}

void HamXuat(float a[], int n)
{
	cout << "Mang so thuc ban vua nhap la: " << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl <<endl;
}

void HamXuatSoLonNhat(float a[], int n)
{
	float max = a[1];
	for (int i = 1; i <= n; i++)
	{
		if (a[i] > max)
			max = a[i];//max =a[i] chu ko dc ghi a[i]=max =))	
	}
	cout << "Gia tri lon nhat la:" << max;
	cout << endl;
}
*/

//Assignment11_6
/*Viết hàm tìm giá trị dương đầu tiên trong mảng 1 chiều các số thực. Nếu
mảng không có giá trị dương thì trả về - 1 */

/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
float HamXuatGiaTriDuongDauTien(float a[], int n);

int main()
{
	float a[MAX];
	int n;
	float k;
	HamNhap(a, n);
	HamXuat(a, n);
	k=HamXuatGiaTriDuongDauTien(a, n);
	cout << k << endl;
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(float a[], int n)
{
	cout << "Mang ban vua nhap la: " << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl << endl;
}

float HamXuatGiaTriDuongDauTien(float a[], int n)//Nhớ hứng giá tri của hàm =))
{
	cout << "Gia tri duong dau tien cua mang la: ";
	for (int i = 1; i <= n; i++)
	{
		if (a[i] > 0)
			return a[i];
	}
	return -1;
}
*/

//Assignment11_7
/*Tìm số chẵn cuối cùng trong mảng 1 chiều các số nguyên. Nếu mảng
không có giá trị chẵn thì trả về - 1*/
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
int HamSoChanCuoiCung(int a[], int n);

int main()
{
	int a[MAX];
	int n, k;
	HamNhap(a, n);
	HamXuat(a, n);
	k = HamSoChanCuoiCung(a, n);
	cout << k << endl;
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(int a[], int n)
{
	cout << "Mang so nguyen ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';//ghi nham /t =))
	}
	cout << endl << endl;
}

int HamSoChanCuoiCung(int a[], int n)
{
	cout << "So chan cuoi cung cua mang la: ";
	for (int i = n; i >=0; i--)//ko ghi la i=n-1 
	{
		if (a[i] % 2 == 0)
			return a[i];
	}
	return -1;
}
*/

//Assignment11_8
/*Tìm 1 vị trí mà giá trị tại vị trí đó là giá trị nhỏ nhất trong mảng 1 chiều các
số thực*/

/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
int HamTimViTri(float a[], int n);

int main()
{
	float a[MAX];
	int n, k;
	HamNhap(a, n);
	HamXuat(a, n);
	k=HamTimViTri(a, n);
	cout << k << endl;
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
	cout << endl;
}

void HamXuat(float a[], int n)
{
	cout << "Mang so thuc ban vua nhap la: " << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}

int HamTimViTri(float a[], int n)
{
	int Min=1;
	cout << "Vi tri ma tai vi tri do gia tri la nho nhat la vi tri: ";
	for ( int i = 1; i <= n; i++)
	{
		if (a[Min]>a[i])
		{
			Min = i;//ep vao trong day dc
		}
	}
	return Min;
}
*/


//Assignment11_9
/*Tìm vị trí của giá trị chẵn đầu tiên trong mảng 1 chiều các số nguyên. Nếu
mảng không có giá trị chẵn thì sẽ trả về - 1*/
//Phai cach dau coment ra
/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;

void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
int TimViTriGiaTriChan(int a[], int n);

int main()
{
	int a[MAX];
	int n, k;
	HamNhap(a, n);
	HamXuat(a, n);
	k=TimViTriGiaTriChan(a, n);
	cout << k << endl;
	system("pause");
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n: ";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang ban vua nhap la:";
	cout << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl << endl;
}

int TimViTriGiaTriChan(int a[], int n)
{
	cout << "Vi tri gia tri chan dau tien: ";
	for (int i = 1; i <= n; i++)
	{
		if (a[i] % 2 == 0 && a[i] > 0)
			return i;
	}
	return -1;
}
*/

//Assignment11_10 CHƯA XONG
/*Tìm vị trí số hoàn thiện cuối cùng trong mảng 1 chiều các số nguyên. Nếu
mảng không có số hoàn thiện thì trả về giá trị - 1*/
/*Số hoàn thiện là một số nguyên dương mà tổng các ước nguyên dương của nó(số nguyên dương
chia hết cho nó) bằng chính nó*/

/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;

void HamNhap(int a[], int &n);
void HamXuat(int a[], int n);
int SoHoanThien(int n);
int ViTriHoanThienCuoi(int a[], int n);

int main()
{
	int a[MAX];
	int n, k;
	HamNhap(a, n);
	HamXuat(a, n);
	SoHoanThien(n);
	k=ViTriHoanThienCuoi(a, n);
	cout << k << endl;
	system("pause");
	
}

void HamNhap(int a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(int a[], int n)
{
	cout << "Mang so nguyen ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl;
}
int SoHoanThien(int n)
{
	int tong = 0;
	for (int i = 1; i <= n; i++)
	{
		if (n%i == 0)
			tong = tong + i;
		if (tong == n)
			return 1;
	}
	return 0;
}
int ViTriHoanThienCuoi(int a[], int n)
{
	cout << "Vi tri hoan thien cuoi cung la:" << endl;
	for (int i = n; i >= 0; i--)
	{
		if (SoHoanThien(a[i] == 1))
			return i;
	}
	return -1;
}
*/

//Assignment11_11
/*Hãy tìm giá trị dương nhỏ nhất trong mảng 1 chiều các số thực. Nếu mảng
không có giá trị dương thì sẽ trả về - 1*/

/*
#include<iostream>
#include<cmath>

#define MIN 0
#define MAX 100

using namespace std;
void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
void TimGiaTriDuongNhoNhat(float a[], int n);

int main()
{
	float a[MAX];
	int n;
	HamNhap(a, n);
	HamXuat(a, n);
	TimGiaTriDuongNhoNhat(a, n);
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n:";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(float a[], int n)
{
	cout << "Mang so thuc ban vua nhap la:" << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl << endl;
}

void TimGiaTriDuongNhoNhat(float a[], int n)
{
	cout << "Gia tri duong nho nhat la: ";
	float min = a[1];
	for (int i = 1; i <= n; i++)
	{
		if (a[i] < min && a[i]>0)
			min = a[i];
	}
	cout << min << endl;
}
*/

//Assignment11_12
/* Hãy tìm vị trí giá trị dương nhỏ nhất trong mảng 1 chiều các số thực. Nếu
mảng không có giá trị dương thì trả về - 1*/

/*
#include<iostream>
#include<cmath>

#define MAX 100
#define MIN 0

using namespace std;

void HamNhap(float a[], int &n);
void HamXuat(float a[], int n);
int TimViTriGiaTriDuongNhoNhat(float a[], int n);

int main()
{
	float a[MAX];
	int n, k;
	HamNhap(a, n);
	HamXuat(a, n);
	k=TimViTriGiaTriDuongNhoNhat(a, n);
	cout << k << endl;
	system("pause");
}

void HamNhap(float a[], int &n)
{
	cout << "Dieu kien(0<n<100)" << endl;
	cout << "Nhap vao so luong phan tu n: ";
	cin >> n;
	cout << endl;

	if (n<MIN || n>MAX)
	{
		do
		{
			cout << "Xem lai dieu kien" << endl;
			cout << "Moi ban nhap lai n: ";
			cin >> n;
			cout << endl;
		} while (n<MIN || n>MAX);
	}

	for (int i = 1; i <= n; i++)
	{
		cout << "a[" << i << "]=";
		cin >> a[i];
	}
}

void HamXuat(float a[], int n)
{
	cout << "Mang ban vua nhap la:";
	cout << endl;
	for (int i = 1; i <= n; i++)
	{
		cout << a[i] << '\t';
	}
	cout << endl << endl;
}

int TimViTriGiaTriDuongNhoNhat(float a[], int n)
{
	cout << "Vi tri gia tri duong nho nhat la: ";
	//ko nen de thua cau lenh
	int Vitri = 1;//[]
	for (int i = 1; i <= n; i++)
	{
		if ((a[i] > 0) && (a[i] < a[Vitri]))//Phai them ngoac tron
			Vitri = i;
	}
	return Vitri;
	
}
*/


