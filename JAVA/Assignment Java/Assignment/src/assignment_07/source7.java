//Assignment7_1
/*Nhập vào N, khởi tạo mảng tạo mảng số nguyên là các số ngẫu nhiên từ 0 đến N,
tìm giá trị lớn nhất trong mảng*/
/*
package assignment_07;
import java.util.*;
public class source7 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		Random Random=new Random();
		
		int size;
		System.out.print("Nhập vào số lượng phần tử N : ");
		size=Input.nextInt();
		
		int a[]=new int[size];
		int max=a[0];
		for (int i=0;i<size;i++)
		{
			a[i]=Random.nextInt(size);
			System.out.println("a["+i+"]="+a[i]);
			if(a[i]>max)
			max=a[i];
		}
		System.out.print("Số lớn nhất trong mảng là : " + max);

	}

}
*/


//Assignment7_2
/*Nhập vào N, khởi tạo mảng tạo mảng số nguyên là các số ngẫu nhiên từ 0 đến N,
tìm giá trị LẺ lớn nhất trong mảng*/
/*
package assignment_07;
import java.util.*;
public class source7 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		Random Random=new Random();
		
		int size;
		System.out.print("Nhập vào số lượng phần tử N : ");
		size=Input.nextInt();
		
		int a[]=new int[size];
		int max=a[0];
		for (int i=0;i<size;i++)
		{
			a[i]=Random.nextInt(size);
			System.out.println("a["+i+"]="+a[i]);
			if((a[i]%2!=0)&&(a[i]>max))
			max=a[i];
		}
		System.out.print("Số lẻ lớn nhất trong mảng là : " + max);

	}

}
*/

//Assignment7_3
/*Khởi tạo mảng các số từ 1 đến 100, xuất ra kết quả các giá trị từ cuối đến đầu
mảng*/
/*
package assignment_07;
import java.util.*;
public class source7 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		//Random Random=new Random();
		
		int size;
		System.out.print("Nhập vào số lượng phần tử N : ");
		size=Input.nextInt();
		
		int a[]=new int[size];
		int k=100;
		for (int i=0;i<size;i++)
		{
			a[i]=k;
			System.out.println("a["+i+"]="+a[i]);
			k--;
		}
	}
}
*/


//Assignment7_4
/*Nhập vào N, N là số chẵn, khởi tạo mảng tạo mảng số nguyên là các số ngẫu
nhiên từ 0 đến N, xuất ra phần tử ở giữa mảng.*/
/*
package assignment_07;
import java.util.*;
public class source7{
	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		Random Random=new Random();
		
		int size;
		System.out.println("Điều kiên N là số chẵn và không âm");
		System.out.print("Mời bạn nhập vào số N :");
		size=Input.nextInt();
		
		if((size<0)||(size%2!=0))
		{
			do
			{
				System.out.println("Mời bạn xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n");
			}while((size<0)||(size%2!=0));
		}
		int a[]=new int[size];
		int k=size/2;
		for(int i=0;i<size;i++)
		{
			a[i]=Random.nextInt(size);
			System.out.println("a["+i+"]="+a[i]);
			
		}
		System.out.print("Xuất ra phần tử ở giữa mảng : "+"a["+k+"]="+a[k]);
		
	}
}
*/