//Assignment6_1
/*Viết hàm nhập vào số N, khởi tạo mảng các giá trị từ 0 đến N, xuất kết quả giá trị
của mảng*/

/*
package assignment_06;
import java.util.*;
public class source6 {

	public static void main(String[] args) {
		Scanner Input= new Scanner(System.in);
		
		System.out.print("Nhập vào số lượng phần tử N : ");
		int size=Input.nextInt();
		
		int a[]=new int[size];

		for(int i=0;i<size;i++)
		{
			a[i]=i;
			System.out.println("a[" + i+"]="+a[i]);
		}	
	}
}
*/

//Assignment6_2
/*Viết hàm nhập vào số N, khởi tạo mảng các giá trị lẻ từ 0 đến N, xuất kết quả vị trí
của các giá trị có trong mảng*/
/*
package assignment_06;
import java.util.*;
public class source6{
	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập vào số lượng phần tử N : ");
		int size=Input.nextInt();
		
		int a[]=new int[size];
		int k=0;
		for(int i=0;i<size;i++) {
			a[i]=i;
				if(a[i]%2!=0) {
					System.out.println("a["+ k +"]="+a[i]);
					k++;
			}	
		}
	}
}
*/


//Assignment6_3
/*Khởi tạo mảng tạo mảng số nguyên là các số ngẩu nhiên từ 0 đến 100, tính tổng
các giá trị có trong mảng, xuất kết quả*/
/*
package assignment_06;
import java.util.*;
public class source6{
	public static void main(String[] agrs) {
		Random Random=new Random();
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập vào số lượng phần tử N : ");
		int size=Input.nextInt();
		
		int a[]=new int[size];
		int s=0;
		for(int i=0;i<size;i++) {
			a[i]=Random.nextInt(101);
			System.out.println("a["+i+"]="+a[i]);
			s=s+a[i];
		}
		System.out.print("Tổng là : "+s);
	}
}
*/


//Assignment6_4
/*Khởi tạo mảng tạo mảng số nguyên là các số ngẩu nhiên từ 0 đến 100, tính tổng
các giá trị ở vị trí lẻ trong mảng, xuất kết quả*/
/*
package assignment_06;
import java.util.*;
public class source6{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		Random Random=new Random();
		

		System.out.print("Nhập vào số lượng phần tử N : ");
		int size=Input.nextInt();
		
		int a[]=new int[size];
		int s=0;
		for(int i=0;i<size;i++) {
			a[i]=Random.nextInt(101);
			System.out.println("a["+i+"]="+a[i]);
			if(i%2!=0)
			s=s+a[i];
		}
		System.out.println("Tổng các giá trị ở vị trí lẻ là : "+s);
	}
}
*/


//Assignment6_5
/*Nhập vào N, khởi tạo mảng tạo mảng số nguyên là các số ngẩu nhiên từ 0 đến N,
tính tổng các giá trị > 10 trong mảng, xuất kết quả*/
/*
package assignment_06;
import java.util.*;
public class source6{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		Random Random=new Random();
		
		System.out.print("Nhập vào số lượng phần tử N : ");
		int size=Input.nextInt();
		
		int s=0;
		int a[]=new int[size];
		for(int i=0;i<size;i++) {
			a[i]=Random.nextInt(size);
			System.out.println("a["+i+"]="+a[i]);
			if(a[i]>10)
				s=s+a[i];
		}
		System.out.print("Tổng các giá trị lớn hơn 10 trong mảng : "+s);
	}
}
*/