package assignment_13;
import java.util.Scanner;
public class Singer {
Scanner Input = new Scanner(System.in);

		private String name;
		private int birthday;
		private String sex;
		private int age;
		private String company;
		
			protected void enterInformation() {
				System.out.print("Enter name: ");
					name = Input.nextLine();
					
				System.out.print("Enter birthday: ");
					birthday = Input.nextInt();
					
				System.out.print("Enter sex: ");
					sex = Input.nextLine();
					
				System.out.print("Enter age: ");
					age = Input.nextInt();
					
				System.out.print("Enter company: ");
					company = Input.nextLine();
			}
			
			
			protected void showInfoSinger() {
				System.out.println("Name: " + this.name);
				System.out.println("Birthday: " + this.birthday);
				System.out.println("Sex: " + this.sex);
				System.out.println("Age: " + this.age);
				System.out.println("Company" + this.company);
			}
			
			public void showTheBehaviorSinger() {
				System.out.println("TheBehaviorSinger \n-Song \n-Eat \n-Move");
			}
}
