package assignment_13;
import java.util.Scanner;
public class Message {
Scanner Input = new Scanner(System.in);

	private String time;
	private int date;
	private String theme;
		
		protected void enterInformation() {
			System.out.print("Enter time: ");
				time = Input.nextLine();
				
			System.out.print("Enter date: ");
				date = Input.nextInt();
				
			System.out.print("Enter theme: ");
				theme = Input.nextLine();		
		}
		
		
		protected void showInfoMessage() {
			System.out.println("Time: " + this.time);
			System.out.println("Date: " + this.date);
			System.out.println("Theme: " + this.theme);
		}
		
		public void showTheBehaviorMessage() {
			System.out.println("TheBehaviorMessage \n-Is sent \n-Receive \n-Contains content");
		}
}
