package assignment_13;

public class assignment13 {
	
	public static void main(String[] agrs) {
	Student Student = new Student();
	System.out.println("-----Show Info Student-----");	
		Student.enterInformation();
		Student.showInfoStudent();
		Student.showTheBehaviorStudent();
		
		
	Singer Singer = new Singer();
	System.out.println("-----Show Info Singer----- ");
		Singer.enterInformation();
		Singer.showInfoSinger();
		Singer.showTheBehaviorSinger();
		
	Doctor Doctor = new Doctor();
	System.out.println("-----Show Info Doctor-----");
		Doctor.enterInformation();
		Doctor.showInfoHospital();
		Doctor.showTheBehaviorStudent();
		
		
	Song Song = new Song();
	System.out.println("-----Show Info Song-----");
		Song.enterInformation();
		Song.showInfoSong();
		Song.showTheBehaviorHospital();
		
		
	Drug Drug = new Drug();
	System.out.println("-----Show Info Drug-----");
		Drug.enterInformation();
		Drug.showInfoDrug();
		Drug.showTheBehaviorDrug();
		
	Subjects Subjects = new Subjects();
	System.out.println("-----Show Info Subjects-----");
		Subjects.enterInformation();
		Subjects.showInfoSubjects();
		Subjects.showTheBehaviorSubjects();
		
	MotelRoom MotelRoom = new MotelRoom();
	System.out.println("-----Show Info MotelRoom-----");
		MotelRoom.enterInformation();
		MotelRoom.showInfoMotelRoom();
		MotelRoom.showTheBehaviorMotelRoom();
		
	Employees Employees = new Employees();
	System.out.println("-----Show Info Employees-----");
		Employees.enterInformation();
		Employees.showInfoEmployess();
		Employees.showTheBehaviorEmployees();
		
		
	Message Message = new Message();
	System.out.println("-----Show Info Message-----");
		Message.enterInformation();
		Message.showInfoMessage();
		Message.showTheBehaviorMessage();
		
	Hospital Hospital = new Hospital();
	System.out.println("-----Show Info Hospital-----");
		Hospital.enterInformation();
		Hospital.showInfoHospital();
		Hospital.showTheBehaviorHospital();
	}
}