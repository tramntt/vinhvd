package assignment_13;
import java.util.Scanner;
public class Doctor {
Scanner Input = new Scanner(System.in);

	private String name;
	private int birthday;
	private String sex;
	private int age;
	private String hospital;
	
	public void enterInformation() {
		
		System.out.print("Enter name: ");
			name = Input.nextLine();
			
		System.out.print("Enter birthday: ");
			birthday = Input.nextInt();
			
		System.out.print("Enter sex: ");
			sex = Input.nextLine();
			
		System.out.print("Enter age: ");
			age = Input.nextInt();
			
		System.out.print("Enter hospital: ");
			hospital = Input.nextLine();
	}
	
	public void showInfoHospital() {
		System.out.println("Name: " + this.name);
		System.out.println("Birthday: " + this.birthday);
		System.out.println("Sex: " + this.sex);
		System.out.println("Age: " + this.age);
		System.out.println("Hospital: " + this.hospital);
	}
	
	public void showTheBehaviorStudent() {
		System.out.println("TheBehaviorStudent \n-Eat \n-Drink \n-Medical examination");
	}
}
