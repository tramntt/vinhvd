package assignment_13;
import java.util.Scanner;
public class Song {
Scanner Input = new Scanner(System.in);

	private String name;
	private String author;
	private String typeoffile;
	private String singer;
		
		protected void enterInformation() {
			System.out.print("Name: ");
				name = Input.nextLine();
				
			System.out.print("Author: ");
				author = Input.nextLine();
				
			System.out.print("Type of file: ");
				typeoffile = Input.nextLine();
				
			System.out.print("Singer: ");
				singer = Input.nextLine();
		}
		
		protected void showInfoSong() {
			System.out.println("Name: "+ this.name);
			System.out.println("Author: " + this.author);
			System.out.println("TypeOfFile: " + this.typeoffile);
			System.out.println("Singer: " + this.singer);
		}
		
		public void showTheBehaviorHospital() {
			System.out.println("TheBehaviorHospital \nIs open \nBe closed \nBe upgraded");
		}
}
