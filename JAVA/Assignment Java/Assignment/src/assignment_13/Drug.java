package assignment_13;
import java.util.Scanner;
public class Drug {
Scanner Input = new Scanner(System.in);

	private String name;
	private String company;
	private String ingredient;
	private String dateofmanufacture;
	private String expirationdate;
	
			protected void enterInformation() {
				
				System.out.print("Enter name: ");
					name = Input.nextLine();
					
				System.out.print("Enter company: ");
					company = Input.nextLine();
					
				System.out.print("Enter ingredient: ");
					ingredient = Input.nextLine();
					
				System.out.print("Enter dateofmanufacture: ");
					dateofmanufacture = Input.nextLine();
					
				System.out.print("Enter expirationdate: ");
					expirationdate = Input.nextLine();
					
			}
			
			protected void showInfoDrug() {
				System.out.println("Name: " + this.name);
				System.out.println("Company: " + this.company);
				System.out.println("Ingredient: " + this.ingredient);
				System.out.println("DateOfManufacture: " + this.dateofmanufacture);
				System.out.println("ExpirationDate: " + this.expirationdate);
				
			}
			
			public void showTheBehaviorDrug() {
				System.out.println("TheBehaviorDrug \n-Sold (bán) \n-Is produced (sản xuất) \n-Shipped (vận chuyển)");
		
			}

}
