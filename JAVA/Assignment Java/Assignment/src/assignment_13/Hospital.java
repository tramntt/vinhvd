package assignment_13;
import java.util.Scanner;
public class Hospital {
Scanner Input = new Scanner(System.in);

	private String name;
	private String address;
	private int founding;
	
		protected void enterInformation() {
			System.out.print("Enter name: ");
				name = Input.nextLine();
				
			System.out.print("Enter address: ");
				address = Input.nextLine();
				
			System.out.print("Enter founding: ");
				founding = Input.nextInt();
		}
		
		
		protected void showInfoHospital() {
			System.out.println("Name: " + this.name);
			System.out.println("Address: " + this.address);
			System.out.println("Founding: " + this.founding);
		}
		
		public void showTheBehaviorHospital() {
			System.out.println("TheBehaviorHospital \nIs open \nBe closed \nBe upgraded");
		}
}
