package assignment_13;
import java.util.Scanner;
public class MotelRoom {
Scanner Input = new Scanner(System.in);

	private String name;
	private String address;
	private int apartmentNumber;
	private String street;
	private String money;
	
		protected void enterInformation() {
			System.out.print("Enter name: ");
				name = Input.nextLine();
				
			System.out.print("Enter address: ");
				address = Input.nextLine();
				
			System.out.print("Enter apartmentnumber: ");
				apartmentNumber = Input.nextInt();
				
			System.out.print("Enter street: ");
				street = Input.nextLine();
				
			System.out.print("Enter money: ");
				money = Input.nextLine();
		}
		protected void showInfoMotelRoom() {
			System.out.println("Name: "+ this.name);
			System.out.println("Address: " + this.address);
			System.out.println("Apartment Number: " + this.apartmentNumber);
			System.out.println("Street: " + this.street);
			System.out.println("Money: "  + this.money);
		}
		
		public void showTheBehaviorMotelRoom() {
			System.out.println("TheBehaviorMotelRoom \n-Is open \n-Be closed \n-Be upgraded");
		}
}
