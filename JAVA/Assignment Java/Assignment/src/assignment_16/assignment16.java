package assignment_16;

public class assignment16 {
	public static void main(String[] agrs) {
		Circle Circle = new Circle();
		Circle.setRadius(2);
		Circle.showGetInfoCircle();
		
		
		Point Point = new Point();
		Point.setPoint(2);
		Point.showGetInfoPoint();
		
		
		Rectangle Rectangle = new Rectangle();
		Rectangle.setLength(2);
		Rectangle.setWidth(4);
		Rectangle.showGetInfoRectangle();
		
		
		Student Student = new Student();
		Student.setName("Tatsuryo");
		Student.setAge(2000);
		Student.setBirthday(1000);
		Student.setSex("Male");
		Student.showGetInfoStudent();
		
		
		Triangle Triangle = new Triangle();
		Triangle.setAlpha(1);
		Triangle.setBeta(2);
		Triangle.setGamma(3);
		Triangle.showGetInfoTriangle();
	}
	
}