package assignment_16;

import java.util.*;

public class Point {
	Scanner Input = new Scanner(System.in);

	private float point;

		public float getPoint() {
			return point;
		}
	
		public void setPoint(float point) {
			this.point = point;
		}
		
	public void showGetInfoPoint() {
		System.out.println("----------Point-----------");
		System.out.println("Point: " + this.getPoint());
	}

	
}
