package assignment_16;

import java.util.*;

public class Triangle {
	Scanner Input = new Scanner(System.in);

	private float alpha;
	private float beta;
	private float gamma;

		public float getAlpha() {
			return alpha;
		}
	
		public void setAlpha(float alpha) {
			this.alpha = alpha;
		}
	
		public float getBeta() {
			return beta;
		}
	
		public void setBeta(float beta) {
			this.beta = beta;
		}
	
		public float getGamma() {
			return gamma;
		}
	
		public void setGamma(float gamma) {
			this.gamma = gamma;
		}
		
		
	public void showGetInfoTriangle() {
		System.out.println("----------Triangle-----------");
		System.out.println("Alpha: " + this.getAlpha());
		System.out.println("Beta: " + this.getBeta());
		System.out.println("Gamma: " + this.getGamma());
	}



}
