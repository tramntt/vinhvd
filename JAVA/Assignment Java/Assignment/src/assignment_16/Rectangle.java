package assignment_16;

import java.util.*;

public class Rectangle {
	Scanner Input = new Scanner(System.in);

	private double length;
	private double width;

		public double getLength() {
			return length;
		}
	
		public void setLength(double length) {
			this.length = length;
		}
	
		public double getWidth() {
			return width;
		}
	
		public void setWidth(double width) {
			this.width = width;
		}
		
	public void showGetInfoRectangle() {
		System.out.println("----------Rectangle-----------");
		System.out.println("Width: " + this.getWidth());
		System.out.println("Length: " + this.getLength());
	}

}
