package assignment_16;

import java.util.*;

public class Student {
	Scanner Input = new Scanner(System.in);

	private String name;
	private int birthday;
	private String sex;
	private int age;

		public String getName() {
			return name;
		}
	
		public void setName(String name) {
			this.name = name;
		}
	
		public int getBirthday() {
			return birthday;
		}
	
		public void setBirthday(int birthday) {
			this.birthday = birthday;
		}
	
		public String getSex() {
			return sex;
		}
	
		public void setSex(String sex) {
			this.sex = sex;
		}
	
		public int getAge() {
			return age;
		}
	
		public void setAge(int age) {
			this.age = age;
		}

	public void showGetInfoStudent() {
		System.out.println("----------Student-----------");
		System.out.println("Name: " + this.getName());
		System.out.println("Age: " + this.getAge());
		System.out.println("Birthday: " + this.getBirthday());
		System.out.println("Male: " + this.getSex());
	}

}
