
//Assignment3_1
//Viết hàm nhập vào ba số, tính tích của ba số và xuất kết quả
/*
package assignment_03;

import java.util.Scanner;

import java.lang.Math;

public class source3 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int size;
		System.out.print("Nhập vào số lượng phần tử n là : ");
		size = input.nextInt();

		int a[] = new int[size];

		for (int i = 0; i < size; i++) {
			System.out.print("a[" + i + "]=");
			a[i] = input.nextInt();
		}

		System.out.println("Mảng bạn vừa nhập là : ");
		for (int i = 0; i < size; i++) {
			System.out.print(a[i]);
			System.out.print("\t");
		}
		
		int s=0;
		for(int i=0;i<size;i++) {
			s+=a[i];
		}
		System.out.print("\r\n");
		System.out.println("Tổng là : "+s);
	}
}
*/

//Assignment3_2
/*Viết hàm nhập vào ba số, tính tổng của số đầu và số cuối và xuất kết quả*/
/*
package assignment_03;

import java.util.Scanner;

public class source3{
	public static void main(String[] args) {
		
		Scanner Input=new Scanner(System.in);
		float a,b,c,s;
		System.out.print("Nhập vào số a : ");
		a=Input.nextFloat();
		
		System.out.print("Nhập vào số b : ");
		b=Input.nextFloat();
		
		System.out.print("Nhập vào số c : ");
		c=Input.nextFloat();
		
		s=a+c;
		
		System.out.print("Tổng hai số đầu và số cuối là : " +s);
		
	}
}
*/


//Assignment3_3
//Viết hàm nhập vào N, N < 100, tính tích từ 0 đến N, xuất kết quả
/*
package assignment_03;

import java.util.Scanner;
import java.lang.Math;

public class source3{
	public static void main(String[] args) {
		final int MIN=0;
		final int MAX=100;
		
		Scanner Input=new Scanner(System.in);
		
		int size;
		System.out.println("Điều kiện(0<n<100)");
		System.out.print("Nhập vào phần tử n : ");
		size=Input.nextInt();
		
		if(size<MIN || size > MAX)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Moi bạn nhập lại phần tử n : ");
				size=Input.nextInt();
				System.out.print("\r\n");
			}while(size<MIN || size>MAX);
		}
	
		int s=1;
		System.out.print("Tích từ 1 tới n là : ");
		for(int i =1;i<=size;i++)
		{
			s*=i;
		}
		System.out.println(s);
	}
}
*/


//Assignment3_4
//Viết hàm nhập vào N, N &lt; 100, tính tổng các số chẳn từ 0 đến N
/*
package assignment_03;

import java.util.Scanner;

public class source3{
	public static void main(String[] args) {
		final int MIN=0;
		final int MAX=100;
		
		Scanner Input= new Scanner(System.in);
		int size;
		System.out.println("Điều kiện (0<n<100)");
		System.out.print("Nhap vào phần tử n : ");
		size=Input.nextInt();
		
		if(size<MIN || size >MAX)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại phần tử n : ");
				size=Input.nextInt();
				System.out.print("\r\n");
			}while (size<MIN || size >MAX);
		}
		int s=0;
		System.out.print("Tổng các số chẵn từ 0 tới n là : ");
		for (int i=0;i<size;i++)
		{
			if(i%2==0)
			{
				s=s+i;
			}
		}
		System.out.print(s);
	}
}
*/


//Assignment3_5
/*Viết hàm nhập vào N, N &lt; 100, tính tổng các số chia hết cho 5 từ 0 đến N*/
/*
package assignment_03;

import java.util.Scanner;

public class source3{
	public static void main(String[] args) {
		final int MIN=0;
		final int MAX=100;
		
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.println("Điều kiện (0<n<100)");
		System.out.print("Nhập vào phần tử n : ");
		size=Input.nextInt();
		
		if(size<MIN || size>MAX)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại phần tử n : ");
				size=Input.nextInt();
				System.out.print("\r\n");
			}while(size<MIN || size>MAX);
		}
		
		System.out.print("Tổng các số chia hết cho 5 từ 0 tới n là : ");
		int s=0;
		for(int i=0;i<=size;i++)
		{
			if(i%5==0)
				s=s+i;
		}
		System.out.print(s);
		
		
	}
	
}
*/


//Assignment3_6
/*Nhập vào 1 ký tự trong các ký tự A, B, C, D, E. Xuất ra ký tự in thường của ký tự
nhập vào*/
/*
package assignment_03;

import java.util.Scanner;

public class source3{
	public static void main(String[] args){
		Scanner Input=new Scanner(System.in);
		char a;
		System.out.println("Bạn chỉ được nhập từ kí tự hoa A đến kí tự E trong bảng chữ cái (A,B,C,D,E)");
		System.out.println("Giới hạn trong bảng mã ASCII(65<Kítự<69)");
		System.out.print("Nhập vào 1 kí tự : ");
		a=Input.next().charAt(0);
		System.out.print("\r\n");

		if(a<'A' || a>'E')
		{
			do 
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại kí tự : ");
				a=Input.next().charAt(0);
				System.out.print("\r\n");
			}while(a<'A' || a>'E');
		}
		
		System.out.print("Kí tự thường của "+a+" là : ");
		System.out.println(Character.toLowerCase(a));
	}
}
*/

//Assignment3_7
/*Nhập vào 2 ký tự trong các ký tự A, B, C, D, E. Tính tổng mã ascii của hai ký tự
nhập vào, xuất kết quả*/
/*
package assignment_03;

import java.util.Scanner;

public class source3{
	public static void main(String[] args) {
	
		Scanner Input=new Scanner(System.in);
		
		System.out.println("Điều kiện: Bạn chỉ được nhập từ kí tự hoa A đến kí tự E(A,B,C,D,E)");
		System.out.println("Giới hạn trong bảng mã ASCII(65<=KíTự<=69) \r\n");
		
		System.out.print("Nhập vào kí tự thứ nhất : ");
		char a=Input.next().charAt(0);
		System.out.print("Kí tự thứ nhất bạn vừa nhập có bảng mã ASCII là : ");
		System.out.print((int)a);
		System.out.print("\r\n\r\n");
		if(a<'A' || a>'E')
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại kí tự thứ nhất : ");
				a=Input.next().charAt(0);
				System.out.print("Kí tự thứ nhất bạn vừa nhập lại có mã ASCII là : ");
				System.out.print((int)a);
				System.out.print("\r\n\r\n");
			}while(a<'A'||a>'E');
			System.out.print("\r\n");
		}
		
		System.out.print("Nhập vào kí tự thứ hai : ");
		char b=Input.next().charAt(0);
		System.out.print("Kí tự thứ hai bạn vừa nhập có bảng mã ASCII là : ");
		System.out.print((int)b);
		System.out.print("\r\n\r\n");
		if(b<'A' || b>'E')
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại kí tự thứ hai : ");
				b=Input.next().charAt(0);
				System.out.print("Kí tự thứ hai bạn vừa nhập lại có bảng mã ASCII là : ");
				System.out.println((int)b);
				System.out.print("\r\n\r\n");
			}while(a<'A' || b>'E');
			System.out.print("\r\n");
		}
		int ch= (int)a+ (int)b;
		System.out.println("Hai kí tự bạn vừa nhập là : "+a+" và "+b);
		System.out.println("Tổng mã ASCII của hai kí tự "+a+" và "+b+" là : "+ch);
		
	}
}
*/


