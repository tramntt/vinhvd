//Assignment5_1
//Viết hàm nhập vào số N, tính kết quả dãy số : ½ + ⅓ + ¼ + … + 1/n , xuất kết quả
/*
package assignment_05;
import java.util.Scanner;
public class source5 {

	public static void main(String[] args) {
			Scanner Input=new Scanner(System.in);
			int size;
			System.out.print("Nhập vào phần tử n : ");
			size=Input.nextInt();
			
			float s=0;
			for(float i=2;i<=size;i++) 
			{
				s+=1/i;
			}
			System.out.println("Kết quả là : "+s);

	}

}
*/


//Assignment5_2
/*Viết hàm nhập vào số N, tính kết quả dãy số : ½ + ⅔ + ¾ + … + (n-1)/n , xuất kết
quả*/
/*
package assignment_05;
import java.util.Scanner;
public class source5{
	public static void main(String[]  args) {
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.print("Nhập vào phần tử n : ");
		size=Input.nextInt();
		
		float s=0;
		for(float i=2;i<=size;i++)
		{
			s+=(i-1)/i;
		}
		System.out.println("Kết quả là : "+s);
		
	}
}
*/


//Assignment5_3 Chưa xong
/*Viết hàm nhập vào số N, tính kết quả dãy số : ½ + ⅔ - ¾ + ⅘ - ⅚ … + (n-1)/n , xuất
kết quả*/
/*
package assignment_05;
import java.util.Scanner;
public class source5{
	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.println("Nhập vào phần tử n : ");
		size=Input.nextInt();
		
		float s=0;
		float k=0; 
		for(float i=2;i<=size;i++)
		{
			s=s+(i-1)/i;
			if(i%2==0)
				k=s*-1;
		}
		System.out.println("Kết quả là : "+k);
	}
}
*/


//Assignment5_4
//Viết hàm nhập vào số N, tính kết quả dãy số : 1 * 2 * 3 …. * n, xuất kết quả
/*
package assignment_05;
import java.util.Scanner;
public class source5{
	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.print("Nhập vào phần tử n : ");
		size=Input.nextInt();
		
		int s=1;
		for(int i=1;i<=size;i++)
		{
			s=s*i;
		}
		
		System.out.println("Kết quả là : "+s);
	}
}
*/


//Assignment5_5
//Viết hàm nhập vào số N, tính kết quả dãy số : n * n * …. * n, xuất kết quả
/*
package assignment_05;
import java.util.Scanner;
public class source5{
	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.print("Nhập vào số n : ");
		size=Input.nextInt();
		
		int s=1;
		for(int i=1;i<=size;i++ )
		{
			s=s*size;
		}
		System.out.println("Kết quả là : "+s);
	}
}
*/


//Assignment5_6
/*Viết hàm nhập vào số N, tính kết quả dãy số : (1 * n) + (2 * n) + (3 * n) + …. (n * n),
xuất kết quả*/
/*
package assignment_05;
import java.util.Scanner;
public class source5{
	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.print("Nhập vào số n : ");
		size=Input.nextInt();
		
		int s=0;
		for(int i=1;i<=size;i++) {
			s+=(i*size);
		}
		System.out.println("Kết quả là : "+s);
	}
}
*/


//Assignment5_7
/*Viết hàm nhập vào số N, tính kết quả dãy số : (1 * 2) + (2 * 3) + (3 * 4) + …. ((n-1) *
n), xuất kết quả*/
/*
package assignment_05;
import java.util.Scanner;
public class source5{
	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.print("Nhập vào số n : ");
		size=Input.nextInt();
		
		int s=0;
		for(int i =1;i<=size;i++)
		{
			s+=(i-1)*i;
		}
		System.out.println("Kết quả là : "+s);
	}
}
*/
