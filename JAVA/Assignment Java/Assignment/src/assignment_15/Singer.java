package assignment_15;

public class Singer  {
	
	private String name;
	private int age;
	private String sex;
	private String salary;
	private String company;
		
		
		public Singer(String name, int age, String sex, String salary, String company) {
			this.name = name;
			this.age = age;
			this.sex = sex;
			this.salary = salary;
			this.company = company;
		}
			
		public void showGetInfoSinger() {
			System.out.println("---------------Singer--------------");
			System.out.println("Name: " + this.name);
			System.out.println("Age : " + this.age);
			System.out.println("Sex : " + this.sex);
			System.out.println("Salary: " + this.salary);
			System.out.println("Company: " + this.company);
		}
		
		public void showTheBehaviorSinger() {
			System.out.println("TheBehaviorSinger \n-Song \n-Eat \n-Move");
		}
		
		
		
}
