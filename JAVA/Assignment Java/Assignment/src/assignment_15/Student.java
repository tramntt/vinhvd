package assignment_15;

public class Student {
	private String name;
	private int age;
	private String sex;
	private String school; 
	private int studentCode;
	
		
		public Student(String name, int age, String sex, String school, int studentCode) {
			this.name = name;
			this.age = age;
			this.sex = sex;
			this.school = school;
			this.studentCode = studentCode;
		}
		
		public void showGetInfoStudent() {
			System.out.println("---------------Student--------------" );
			System.out.println("Name: " + this.name);
			System.out.println("Age: " + this.age);
			System.out.println("Sex: " + this.sex);
			System.out.println("School: " + this.school);
			System.out.println("StudentCode: " + this.studentCode);			
		}
		
		public void showTheBehaviorStudent() {
			System.out.println("TheBehaviorStudent \n-Eat \n-Drink \n-PlayGame");
		}
	
	
		
		
	}

