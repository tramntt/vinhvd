package assignment_15;

public class Drug {
	private String name;
	private String company;
	private String ingredient;
	private String dateOfManufacture;
	private String expirationDate;
	
		
		
		public Drug(String name, String company, String ingredient, String dateOfManufacture, String expirationDate) {
			this.name = name;
			this.company = company;
			this.ingredient = ingredient;
			this.dateOfManufacture = dateOfManufacture;
			this.expirationDate = expirationDate;
		}
			
		public void showGetInfoDrug() { 
			System.out.println("---------------Drug--------------");
			System.out.println("Name: " + this.name);
			System.out.println("Company: " + this.company);
			System.out.println("Ingredient: " + this.ingredient);
			System.out.println("Date of manu facture: " + this.dateOfManufacture);
			System.out.println("Expiration Date: " + this.expirationDate);
		}
		
		public void showTheBehaviorDrug() {
			System.out.println("TheBehaviorDrug \n-Sold (bán) \n-Is produced (sản xuất) \n-Shipped (vận chuyển)");
	
		}
		
	
}
