package assignment_15;

public class Message {
	private String time;
	private String date;
	private String theme;
		
		public Message(String time, String date, String theme) {
			this.time = time;
			this.date = date;
			this.theme = theme;
		}
		
		public void showGetInfoMessage() {
			System.out.println("---------------Message--------------");
			System.out.println("Time: " + this.time);
			System.out.println("Date: " + this.date);
			System.out.println("Theme: " + this.theme);
		}
		
		public void showTheBehaviorMessage() {
			System.out.println("TheBehaviorMessage \n-Is sent \n-Receive \n-Contains content");
		}
		
		
}
