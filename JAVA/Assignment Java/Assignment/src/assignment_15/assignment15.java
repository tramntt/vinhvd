package assignment_15;

public class assignment15 {

	public static void main(String[] args) {
		/*1. Xác định & cập nhật phạm vi của thuộc tính, hành vi và các lớp trong ASM 14
		  2. Khởi tạo đối tượng cho các lớp trong ASM 14 
		  3. In ra thông tin thuộc tính của đối tượng ở (1)
		  4. In ra kết quả truy cập vào thuộc tính private
		  5. In ra kết quả truy cập vào hành vi private
		 */
	
		Doctor doctor = new Doctor("TatsuRyo",1999,"Male",19,"QWE");
		doctor.showInfoGetDoctor();
		doctor.showTheBehaviorStudent();
		
		Drug drug = new Drug("Ritalin","????","?????","?????","?????");
		drug.showGetInfoDrug();
		drug.showTheBehaviorDrug();
		
		Employees employees = new Employees("TatsuRyo",19,"Male","2000$","TMA");
		employees.showGetInfoEmployees();
		employees.showTheBehaviorEmployees();
		
		Hospital hospital = new Hospital("TMA","????","?????");
		hospital.showGetInfoHospital();
		hospital.showTheBehaviorHospital();
		
		Message message = new Message("????","?????","?????");
		message.showGetInfoMessage();
		message.showTheBehaviorMessage();
		
		MotelRoom motelRoom = new MotelRoom("????","????",123,"????","????");
		motelRoom.showGetInfoMotelRoom();
		motelRoom.showTheBehaviorMotelRoom();
		
		Singer singer = new Singer("?????",12,"?????","?????","?????");
		singer.showGetInfoSinger();
		singer.showTheBehaviorSinger();
		
		Song song = new Song("?????","?????","??????","??????");
		song.showGetInfoSong();
		song.showTheBehaviorSong();
		
		Student student = new Student("?????",19,"Male","TMA",123456);
		student.showGetInfoStudent();
		student.showTheBehaviorStudent();
		
		Subjects subjects = new Subjects("????","?????","??????");
		subjects.showGetInfoSubjects();
		subjects.showTheBehaviorSubjects();
		
	}

}
