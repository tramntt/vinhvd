package assignment_15;

public class Subjects {
	private String name;
	private String houseFloor;
	private String theme;
	
		public Subjects(String name, String houseFloor, String theme) {
			this.name = name;
			this.houseFloor = houseFloor;
			this.theme = theme;
		}
			
			
		public void showGetInfoSubjects() {
			System.out.println("---------------Subjects--------------");
			System.out.println("Name: " + this.name);
			System.out.println("Point: " + this.houseFloor);
			System.out.println("Teacher: " + this.theme);
		}
		
		public void showTheBehaviorSubjects() {
			System.out.println("TheBehaviorSubjects \n-Is produced \n-Printed \nSold");
		}
		

}
