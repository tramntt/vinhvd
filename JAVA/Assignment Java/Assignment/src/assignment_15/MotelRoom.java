package assignment_15;

public class MotelRoom {
	private String name;
	private String address;
	private int apartmentNumber;
	private String street;
	private String money;
	
	
		public MotelRoom(String name, String address, int apartmentNumber, String street, String money) {
			this.name = name;
			this.address = address;
			this.apartmentNumber = apartmentNumber;
			this.street = street;
			this.money = money;
		}
			
		
		public void showGetInfoMotelRoom() {
			System.out.println("---------------MotelRoom--------------");
			System.out.println("Name: " + this.name);
			System.out.println("Address: " + this.address);
			System.out.println("Apartment Number: " + this.apartmentNumber);
			System.out.println("Street: " + this.street);
			System.out.println("Money: " + this.money);
		}
		
		public void showTheBehaviorMotelRoom() {
			System.out.println("TheBehaviorMotelRoom \n-Is open \n-Be closed \n-Be upgraded");
		}
		
		
	
}
