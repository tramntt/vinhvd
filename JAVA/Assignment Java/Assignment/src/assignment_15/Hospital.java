package assignment_15;

public class Hospital {
	private String name;
	private String address;
	private String founding;
		
		public Hospital(String name, String address, String founding) {
			this.name = name;
			this.address = address;
			this.founding = founding;
		}
		
			
		public void showGetInfoHospital() {
			System.out.println("---------------Hospital--------------");
			System.out.println("Name: " + this.name);
			System.out.println("Address: " + this.address);
			System.out.println("Founding: " + this.founding);
		}
		
		public void showTheBehaviorHospital() {
			System.out.println("TheBehaviorHospital \nIs open \nBe closed \nBe upgraded");
		}
	
			
}
