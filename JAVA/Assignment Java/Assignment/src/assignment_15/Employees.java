package assignment_15;

public class Employees {
	private String name;
	private int age;
	private String sex;
	private String salary;
	private String company;
	
		public Employees(String name, int age, String sex, String salary, String company) {
			this.name = name;
			this.age = age;
			this.sex = sex;
			this.salary = salary;
			this.company = company;		
		}
		
		public void showGetInfoEmployees() {
			System.out.println("---------------Employees--------------");
			System.out.println("Name: " + this.name);
			System.out.println("Age: " + this.age);
			System.out.println("Sex: " + this.sex);
			System.out.println("Salary: " + this.salary);
			System.out.println("Company: " + this.company);
		}
		
		public void showTheBehaviorEmployees() {
			System.out.println("TheBehaviorEmployees \n-Eat \n-Work \n-Drink");
		}
		
		
		
		
}
