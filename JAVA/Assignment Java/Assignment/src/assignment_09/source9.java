//Assignment9_1
//Tính S(n) = 1 + 2 + 3 + ... + n
/*
package assignment_09;
import java.util.*;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		int S=0;
		for(int i=0;i<=size;i++)
		{
			S=S+i;
		}
		System.out.print("Tổng là : "+S);
	}

}
*/


//Assignment9_2
//Tính S(n) = 1 + ½ + 1/3 + ... + 1/n
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		int S=0;
		for(int i=0;i<=size;i++)
		{
			S=S+(i*i);
		}
		System.out.print("Tổng là : "+S);
	}
}
*/

//Assignment9_3
//Tính S(n) = 1 + ½ + 1/3 + ... + 1/n
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		float S=0;
		for(float i=1;i<=size;i++)
		{
			S=S+1/i;
		}
		System.out.print("Kết quả là : "+S);
	}
}
*/


//Assignment9_4
//Tính S(n) = ½ + ¼ + ... + 1/2n
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		float S=0;
		for(float i=1;i<=size;i++)
		{
			S=S+(1/(2*i));
		}
		System.out.print("Tổng là : "+S);
	}
}
*/


//Assignment9_5
//Tính S(n) = 1 + 1/3 + 1/5 + ... + 1/(2n + 1)
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		float S=0;
		for(float i=0;i<=size;i++)
		{
			S=S+(1/((2*i)+1));
		}
		System.out.print("Tổng là : "+S);
	}
}
*/


//Assignment9_6
//Tính S(n) = 1/(1x2) + 1/(2x3) +...+ 1/n x (n + 1)
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		float S=0;
		for(float i=1;i<=size;i++)
		{
			S=S+(1/(i*(i+1)));
		}
		System.out.print("Tổng là : "+S);
	}
}
*/


//Assignment9_7
//Tính S(n) = ½ + 2/3 + ¾ + .... + n / n + 1
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		float S=0;
		for(float i=1;i<=size;i++)
		{
			S=S+(i/(i+1));
		}
		System.out.print("Tổng là : "+S);
	}
}
*/


//Assignment9_8
//Tính S(n) = ½ + ¾ + 5/6 + ... + 2n + 1/ 2n + 2
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		int size;
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		float S=0;
		for(float i=0;i<=size;i++)
		{
			S=S+((2*i)+1)/((2*i)+2);
		}
		System.out.print("Tổng là : "+S);
	}
}
*/


//Assignment9_9
//Tính T(n) = 1 x 2 x 3...x N
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		int size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		float S=1;
		for(float i=1;i<=size;i++)
		{
			S=S*i;
		}
		System.out.print("Tổng là : "+S);
	}
}
*/


//Assignment9_10
// Tính T(x, n) = x^n
/*
package assignment_09;
import java.util.*;
import java.lang.Math;
public class source9 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.println("Điều kiện(X>0)");
		System.out.print("Nhập vào X : ");
		int x=Input.nextInt();
		if(x < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại X : ");
				x=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(x < 0);
		}
		
		int size;
		System.out.println("Điều kiện(N>0)");
		System.out.print("Nhập vào N : ");
		size=Input.nextInt();
		if(size < 0)
		{
			do
			{
				System.out.println("Xem lại điều kiện");
				System.out.print("Mời bạn nhập lại N : ");
				size=Input.nextInt();
				System.out.print("\r\n\r\n");
			}while(size < 0);
		}
		
		
		float S=1;
		for(float i=1;i<=size;i++)
		{
			S*=x;
		}
		System.out.print("Tổng là : "+S);
	}
}
*/