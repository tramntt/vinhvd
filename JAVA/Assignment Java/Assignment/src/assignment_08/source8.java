//Assignment8_2
//Viết hàm nhập vào một chuỗi, xuất ra chuỗi in thường của chuỗi nhập vào
/*
package assignment_08;
import java.util.Scanner;
public class source8 {
	public static void main(String[] agrs) {
		String A,B;
		Scanner Input=new Scanner(System.in); 
		System.out.print("Nhập vào chuỗi : ");
		A=Input.nextLine();
		
		System.out.println("Các kí tự có trong chuỗi là:"+A);
		B=A.toLowerCase();
		System.out.print("Các kí thường có trong chuỗi là:"+B);
		
	}

}
*/


//Assignment8_3;
/*Viết hàm nhập vào một chuỗi gồm các từ cách nhau bởi khoản trắng, tách chuỗi
bằng khoản trắng và xuất ra danh sách các từ.*/
/*
package assignment_08;
import java.util.Scanner;
public class source8{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		String A;
		System.out.print("Nhập vào chuỗi : ");
		A=Input.nextLine();
		for(String w:A.split("\\s", 0))
		{
			System.out.println(w);
		}
	}
}
*/


//Assignment8_4
/*Viết hàm nhập vào một chuỗi, xuất ra kq chuỗi gồm các ký tự ngược lại với chuỗi
nhập vào, vd: nhập ABCDE => xuất ra EDCBA*/
/*
package assignment_08;
import java.util.Scanner;
public class source8{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		
		String A;
		System.out.print("Nhập vào chuỗi : ");
		A=Input.nextLine();
		
		String B = new StringBuffer(A).reverse().toString();
		
		System.out.println("Chuỗi bạn vừa nhập là : "+A);
		System.out.print("Chuỗi ngược là : "+B);
	}
}
*/
