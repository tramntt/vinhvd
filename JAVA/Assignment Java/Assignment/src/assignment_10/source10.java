//Assignment10_1
/*Tính S(n) = 1 + 1.2 + 1.2.3 + ... + 1.2.3....N*/
/*
package assignment_10;
import java.util.*;
public class source10 {

	public static void main(String[] args) {
		Scanner Input=new Scanner(System.in);
		System.out.print("Nhập vào số n : ");
		int size=Input.nextInt();
		
		int a[]=new int[size];
		int s=0;
		int k=1;
		for(int i=1;i<=size;i++)
		{
			k=k*i;
			s=s + k;
		}
		System.out.print("Tổng là : "+s);
	}
}
*/


//Assignment10_2
/*Tính S(n) = x + x^2 + x^3 + ... + x^n*/
/*
package assignment_10;
import java.util.*;
public class source10{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		
		
		System.out.print("Nhập vào số X : ");
		int x=Input.nextInt();
		
		System.out.print("Nhập vào số N : ");
		int size=Input.nextInt();
		
		int a[]=new int[size];
		int s=0;
		for(int i=1;i<=size;i++)
		{
			s+=Math.pow(x,i);
		}
		System.out.print("Kết quả : "+s);
	}
}
*/


//Assignment10_3
/*Tính S(n) = x^2 + x^4 + ... + x^2n*/
/*
package assignment_10;
import java.util.*;
public class source10{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập vào số lượng X : ");
		int x=Input.nextInt();
		System.out.print("Nhập vào số lượng N : ");
		int size=Input.nextInt();
		
		int s=0;
		int k;
		for(int i=1;i<=size;i++)
		{
			s+=Math.pow(x,i*2);
		}
		System.out.print("Kết quả : "+s);
	}
}
*/


//Assignment10_4
/*Tính S(n) = x + x^3 + x^5 + ... + x^2n + 1*/
/*
package assignment_10;
import java.util.*;
public class source10{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập số x : ");
		int x=Input.nextInt();
		
		System.out.print("Nhập số n : ");
		int size=Input.nextInt();
				
		int s=0;
		for (int i=0;i<=size;i++)
		{
			s+=Math.pow(x,(2*i)+1);
		}
		System.out.print("Kết quả : "+s);
	}
}
*/
//Assignment10_5
/*Tính S(n) = 1 + 1/(1 + 2) + 1/( 1 + 2 + 3) + ..... + 1/ 1 + 2 + 3 + .... + N*/
/*
package assignment_10;
import java.util.*;
public class source10{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập vào số n : ");
		int size=Input.nextInt();
		
		float s=0,k=0;
		for(float i=1;i<=size; i++)
		{
			k=k+i;
			s=s+(1/k);
		}
		System.out.print("Kết quả là : "+s);
	}
}
*/


//Assignment10_6
//Tính S(n) = x + x^2/(1 + 2 )+ x^3/(1 + 2 + 3) + ... + x^n/1 + 2 + 3 + .... + N
/*
package assignment_10;
import java.util.*;
public class source10{
	public static void main(String[] agrs)
	{
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập vào số x :");
		int x=Input.nextInt();
		
		System.out.print("Nhập vào số N : ");
		int size=Input.nextInt();
		
		float s=0,k=1,l=0;
		for(float i=1;i<=size;i++)
		{
			l=l+i;
			k=(float)Math.pow(x, i);
			s=s+(k/l);
		}
		System.out.print("Kết quả là : "+s);	
	}
}
*/

//Assignment10_7
//Tính S(n) = x + (x^2)/2! + (x^3)/3! + ... + x^n/N!
/*
package assignment_10;
import java.util.*;
public class source10{
	public static void main(String[] agrs) {
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập vào số x : ");
		int x=Input.nextInt();
		
		System.out.print("Nhập vào số N : ");
		int size=Input.nextInt();
		
		float s=0,k=1,l=1;
		for(int i=1;i<=size;i++)
		{
			l=l*i;
			k=(int)Math.pow(x,i);
			s=s+(k/l);
		}
		System.out.print("Kết quả là : "+s);
	}
}
*/