//Assignment2_1
/*Viết hàm khai báo biến với từng kiếu dữ liệu ở (1), khởi tạo vớ giá trị phù hơp. In
ra màn hình console giá trị của biến*/

/*
package assignment_02;
import java.util.*;
public class source2 {
	
	public static void main(String[] args) {
		
		byte a=10;
		System.out.println(a);
		byte b=-10;
		System.out.println(b);
		
		short z=50;
		System.out.println(z);
		short t=-20;
		System.out.println(t);
		
		int n=5;
		System.out.println(n);
		int m=-23;
		System.out.println(m);
		
		long y=100000L;
		System.out.println(y);
		long d=-200000L;
		System.out.println(d);
		
		float q=22.5f;
		System.out.println(q);
		float w=-23.6f;
		System.out.println(w);
		
		double ad=676.3d;
		System.out.println(ad);
		double bd=-545.4d;
		System.out.println(bd);
		
		boolean bo=true;
		System.out.println(bo);
		boolean boo=false;
		System.out.println(boo);
	}
}
*/


//Assignment2_2
/*Viết hàm nhập vào hai số, tính tổng hai số và xuất kết quả*/

/*
package assignment_02;

import java.util.*;

public class source2{
	public static void main(String[] args) {
		Scanner k = new Scanner(System.in);
		float a,b,c;
		System.out.print("Nhập vào số a : ");
		a=k.nextInt();
		System.out.print("Nhập vào số b : ");
		b=k.nextInt();
		c=a+b;
		System.out.println("Tổng hai số là : "+c);
		
	}
}
*/


//Assignment2_3
/*Viết hàm nhập vào hai số, tính thương hai số và xuất kết quả*/
/*
package assignment_02;

import java.util.*;

public class source2{
	public static void main(String[] args) {
	
		Scanner Input=new Scanner(System.in);
		float a,b,c;
		
		System.out.println("Thương hai số a/b" );
		System.out.print("Nhập vào số a : ");
		a=Input.nextFloat();
		
		System.out.print("Nhập vào số b : ");
		b=Input.nextFloat();
		
		c=a-b;
		System.out.print("Thương của hai số a/b là : " +c );
	}
}
*/


//Assignment2_4
/*Viết hàm nhập vào hai số, tính số dư của phép chia hai số và xuất kết quả*/
/*
package assignment_02;

import java.util.*;
import java.lang.Math;

public class source2{
	public static void main(String[] args) {
		int a,b,c;
		Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào số a : ");
		a=Input.nextInt();
		
		System.out.print("Nhập vào số b : ");
		b=Input.nextInt();
		
		c= a % b;
		System.out.print("Số dư của phép chia là : "+c);
	}
}
*/


//Assignment2_5
/*Viết hàm nhập vào hai số, xuất ra kết quả “TONG CHAN” nếu tổng là chẳn, “TONG
LE" nếu tổng là lẻ*/
/*
package assignment_02;

import java.util.*;
import java.lang.Math;

public class source2{
	 public static void main(String[] args)
	{
		 int a,b,c;
		Scanner Input=new Scanner(System.in);
		System.out.print("Nhập vào số a : ");
		a=Input.nextInt();
		System.out.print("Nhập vào số b : ");
		b=Input.nextInt();
		
		c=a+b;
		System.out.print("Tổng là : "+c);
		System.out.print("\r\n");
		
		if(c%2==0)
			System.out.println("TỔNG CHẴN");
		else
			System.out.println("TỔNG LẺ");
		
	}
}
*/


