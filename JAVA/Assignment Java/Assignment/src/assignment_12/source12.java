//Asssignment12_1
/*Hãy liệt kê các số âm trong mảng 1 chiều các số thực*/
/*
package assignment_12;
import java.util.*;
public class source12 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập N: ");
		N = Input.nextInt();
		return N;
	}
	public static float[] NhapXuatMang(int P, float A[]) {
	Scanner Input = new Scanner(System.in);
		A = new float[P];
		System.out.print("\r\n");
		for(int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+":");
			A[i] = Input.nextFloat();
		}
		
		System.out.print("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static void SoAmMangThuc(float[] A, int N) {
		System.out.println("Các số âm mảng số thực là:" );
		for (int i = 0; i < N; i++) {
			if (A[i] < 0) {
				System.out.println("b1[" + i + "]= " + A[i]);
			}
		}
	}
	
	public static void main(String[] args) {
		int N = 0;
		float[]a=null;
		int P = NhapN(N);
		float[] a1=NhapXuatMang(P,a);
		SoAmMangThuc(a1,P);

	}
}	
*/

//Assignment12_2
/*Hãy liệt kê các số trong mảng 1 chiều các số thực thuộc đoạn [x, y] cho
trước*/
/*
package assignment_12;
import java.util.*;
public class source12{
public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập N: ");
		N = Input.nextInt();
		return N;
	}
	public static float[] NhapXuatMang(int P, float A[]) {
	Scanner Input = new Scanner(System.in);
		A = new float[P];
		System.out.print("\r\n");
		for(int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+":");
			A[i] = Input.nextFloat();
		}
		
		System.out.print("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static int NhapX(int x) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap x:");
		x = input.nextInt();
		return x;
	}

	public static int NhapY(int y, int x1) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap Y:");
		y = input.nextInt();
		do {
			if (y <= x1) {
				System.out.println("Moi nhap lai: ");
				y = input.nextInt();
			}
		} while (y <= x1);
		return y;
	}
}
*/

package assignment_12;
import java.util.*;
public class source12{
public static int NhapN(int n) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap n:");
		n = input.nextInt();
		return n;
	}

	public static int[] NhapMangNguyen(int N) {
		Scanner input = new Scanner(System.in);
		int[] a = new int[N];
		System.out.println("Nhap mang a: ");
		for (int i = 0; i < N; i++) {
			a[i] = input.nextInt();
		}
		for (int i = 0; i < N; i++) {
			System.out.println("a[" + i + "] =" + a[i]);
		}
		return a;
	}

	public static float[] NhapMangThuc(int N) {
		Scanner input = new Scanner(System.in);
		float[] b = new float[N];
		System.out.println("Nhap mang b: ");
		for (int i = 0; i < N; i++) {
			b[i] = input.nextFloat();
		}
		for (int i = 0; i < N; i++) {
			System.out.println("b[" + i + "] =" + b[i]);
		}
		return b;
	}

	public static int NhapX(int x) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap x:");
		x = input.nextInt();
		return x;
	}

	public static int NhapY(int y, int x1) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap Y:");
		y = input.nextInt();
		do {
			if (y <= x1) {
				System.out.println("Moi nhap lai: ");
				y = input.nextInt();
			}
		} while (y <= x1);
		return y;
	}

	public static void SoAmMangThuc(float[] b1, int N) {
		System.out.println("cac so am mang so thuc la:" );
		for (int i = 0; i < N; i++) {
			if (b1[i] < 0) {
				System.out.println("b1[" + i + "]" + b1[i]);
			}
		}
	}

	public static void SoThucDoanXY(float[] b1, int N, int x1, int y1) {
		System.out.println("cac so thuc thuoc doan xy la: ");
		for (int i = 0; i < N; i++) {
			if (x1 <= b1[i] && b1[i] <= y1) {
				System.out.println("b1[" + i + "]" + b1[i]);
			}
		}
	}

	public static void SoNguyenChanDoanXY(int[] a1, int N, int x1, int y1) {
		System.out.println("cac so nguyen chan doan xy la:");
		int[] a2 = new int[N];
		for (int i = 0; i < N; i++) {
			if (a1[i] % 2 == 0) {
				a1[i] = a2[i];
			}
			if (x1 <= a2[i] && a2[i] <= y1) {
				System.out.println("a2[" + i + "]" + a2[i]);
			}
		}
	}


	private static int abs(int i) {
		return 0;
	}

	public static void TongPhanTuMang(int[] a1, float[] b1, int N) {
		int T1=0;
		int T2=0;
		for(int i =0; i<N;i++) {
			T1+=a1[i];
			T2+=b1[i];
		}
		System.out.println("tong gia tri mang nguyen la: "+T1+" va mang thuc la: "+T2);
	}
	
	public static int TimSoLe(int N) {
		int M;
		N= abs(N);
		while(N>=10) {
			M=N%10;
			N/=10;
		}
		if(N%2==0)
			return 0;
		return 1;
	}
	public static void TongCacSoNguyenLe(int[] a1, int N) {
		int T3=0;
		for(int i=0; i<N;i++) {
			if(TimSoLe(a1[i])==1) {
				T3+=a1[i];
			}
		}
		System.out.println("tong cac so nguyen bat dau bang so le la: "+T3);
	}
	
	public static int KetThucVoi5(int N) {
		N = abs(N);
		N= N/10;
		int KT5 = N%10;
		if(KT5 == 5)
			return 1;
		return 0;
	}
	
	public static void TongCacSoKetThucBang5(int[] a1, int N) {
		int T4=0;
		for(int i=0; i<N;i++) {
			if(KetThucVoi5(a1[i])==1) {
				T4+=a1[i];
			}
		}
		System.out.println("tong cac so ket thuc bang 5 la: "+T4);
	}
	
	public static void SoCacSoChan(int[] a1, int N) {
		int P=0;
		for(int i=0; i<N;i++) {
			if(a1[i]%2==0) {
				P++;
			}
		}
		System.out.println("So luong cac so chan la: "+P);
	}
	
	public static void SoDuongChiaHet7(int[] a1, int N) {
		int Q=0;
		for(int i=0; i<N;i++) {
			if(a1[i]>0&&a1[i]%7==0) {
				Q++;
			}
		}
		System.out.println("So duong chia het cho 7 la: "+Q);
	}
	
	public static void SoPhanTuX(int[] a1, int N) {
		int K=0;
		for(int i=0; i<N;i++) {
			if(a1[i]=='x') {
				K++;
			}
		}
		System.out.println("So phan tu x la: "+K);
	}
	
	public static void SoTanCungLa5(int[] a1, int N) {
		int H=0;
		for(int i=0; i<N;i++) {
			if(a1[i]%5==0&&a1[i]%2!=0) {
				H++;
			}
		}
		System.out.println("So phan tu tan cung voi 5 la: "+H);
	}
	
	public static void main(String[] args) {
		int n = 0, x = 0, y = 0;
		int N = NhapN(n);
		int[] a1 = NhapMangNguyen(N);
		float[] b1 = NhapMangThuc(N);
		int x1 = NhapX(x);
		int y1 = NhapY(y, x1);
		SoAmMangThuc(b1, N);
		SoThucDoanXY(b1,N,x1,y1);
		SoNguyenChanDoanXY(a1, N, x1, y1);
		TongPhanTuMang(a1,b1,N);
		TongCacSoNguyenLe(a1,N);
		TongCacSoKetThucBang5(a1,N);
		SoCacSoChan(a1,N);
		SoDuongChiaHet7(a1,N);
		SoPhanTuX(a1,N);
		SoTanCungLa5(a1,N);
	}
}