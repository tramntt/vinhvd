//Assignment11_1
/*Viết hàm nhập, xuất mảng 1 chiều các số thực*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static float[] NhapXuatMang(int P, float[] a ) {
	Scanner Input = new Scanner(System.in);
		a= new float[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			a[i] = Input.nextFloat();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+a[i]);
		}
		return a;
	}
	
	public static void main(String[] args) {
		int N = 0;
		float[]a=null;
		int P = NhapN(N);
		float[] a1=NhapXuatMang(P,a);
	}
}
*/

//Assignment11_2
/*Viết hàm nhập, xuất mảng 1 chiều các số nguyên*/
/*
package assignment_11;
import java.util.*;
//Scanner Input = new Scanner(System.in);
public class source11{

	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static int[] NhapXuatMang(int P, int[] A) {
	Scanner Input = new Scanner(System.in);
		A = new int[P];
		System.out.print("\r\n");
		for(int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextInt();
		}
		
		System.out.print("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for(int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static void main(String[] agrs) {
		int n = 0;
		int a[] = null;
		int P=NhapN(n);
		int a1[]=NhapXuatMang(P,a);
	}
}
*/

//Assignment11_3
/*Viết hàm liệt kê các giá trị chẵn trong mảng 1 chiều các số nguyên*/
/*
package assignment_11;
import java.util.Scanner;
public class source11{
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N=Input.nextInt();
		return N;
	}
	
	public static int[] NhapXuatMang(int P, int A[]){
		Scanner Input = new Scanner(System.in);
		A = new int[P];
		System.out.print("\r\n");
		for(int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextInt();
			}
		System.out.print("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for(int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
			}
		return A;
	}	
	
	public static void GiaTriSoNguyenChan(int P, int[] A) {

		System.out.println("Giá trị số nguyên chẵn là:");
		for (int i = 0; i < P; i++) {
			if (A[i] % 2 == 0) {
				System.out.println("A[" + i + "] = " + A[i]);
			}
		}
	}
	
	public static void main(String[] agrs) {
		int n = 0;
		int A[] = null;
		int P=NhapN(n);
		int A1[]=NhapXuatMang(P,A);
		GiaTriSoNguyenChan(P,A1);
	}
}
*/

//Assignment11_4
/*Viết hàm liệt kê các vị trí mà giá trị tại đó là giá trị âm trong mảng 1 chiều
các số thực*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static float[] NhapXuatMang(int P, float[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new float[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextFloat();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static void ViTriCuaSoThucAm(int P, float[] A) {
		System.out.println("Vị trí giá trị số thực âm là:");
		for (int i = 0; i < P; i++) {
			if (A[i] < 0) {
				System.out.println("A[" + i + "]");
			}
		}
	}
	
	public static void main(String[] args) {
		int N = 0;
		float[] A=null;
		int P = NhapN(N);
		float[] A1=NhapXuatMang(P,A);
		ViTriCuaSoThucAm(P,A1);
	}
}
*/

//Assignment11_5
/*Viết hàm tìm giá trị lớn nhất trong mảng 1 chiều các số thực*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static float[] NhapXuatMang(int P, float[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new float[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextFloat();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static void SoThucLonNhat(int P, float[] A) {
		System.out.println("Số thực lớn nhất là: ");
		float Max = Float.MIN_VALUE;
		for (int i = 0; i < P; i++) {
			Max = (A[i] > Max) ? A[i] : Max;
		}
		System.out.println(Max);
	}
	
	public static void main(String[] agrs) {
		int N = 0;
		float[] A=null;
		int P = NhapN(N);
		float[] A1=NhapXuatMang(P,A);
		SoThucLonNhat(P,A1);
	}
}
*/

//Assignment11_6
/*Viết hàm tìm giá trị dương đầu tiên trong mảng 1 chiều các số thực. Nếu
mảng không có giá trị dương thì trả về -1*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static float[] NhapXuatMang(int P, float[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new float[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextFloat();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static float SoThucDuongDauTien(int P, float[] A) {
		for (int i = 0; i < P; i++)
			if (A[i] > 0.0)
				return A[i];
		return -1;
	}
	
	public static void main(String[] agrs) {
		int N = 0;
		float[] A=null;
		int P = NhapN(N);
		float[] A1=NhapXuatMang(P,A);
		float Array=SoThucDuongDauTien(P,A1);
		System.out.println("Số thực dương đầu tiên là: " + Array);
		
	}
	
}
*/

//Assignment11_7
/*Tìm số chẵn cuối cùng trong mảng 1 chiều các số nguyên. Nếu mảng
không có giá trị chẵn thì trả về -1*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static int[] NhapXuatMang(int P, int[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new int[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextInt();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static int SoNguyenChanCuoiCung(int P, int[] A) {
		for (int i = P - 1; i > 0; i--)
			if (A[i] % 2 == 0)
				return A[i];
		return -1;
	}
	
	public static void main(String[] agrs) {
		int N = 0;
		int[] A=null;
		int P = NhapN(N);
		int[] A1=NhapXuatMang(P,A);
		int Array=SoNguyenChanCuoiCung(P,A1);
		System.out.println("Số nguyên chẵn cuối cùng là: " + Array);
		
	}

}
*/

//Assignment11_8
/*Tìm 1 vị trí mà giá trị tại vị trí đó là giá trị nhỏ nhất trong mảng 1 chiều các
số thực*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static float[] NhapXuatMang(int P, float[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new float[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextFloat();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static float ViTriThucNhoNhat(int P, float[] A) {
		float pos = 0;
		int i;
	    float temp;
	    temp = A[0];
	    for (i = 0; i < P; i++)
	    {
	        if (temp > A[i])
	        {
	            temp = A[i];
	            pos = i;
	        }
	    }
	    return (pos+1);
	}
	
	public static void main(String[] agrs) {
		int N = 0;
		float[] A=null;
		int P = NhapN(N);
		float[] A1=NhapXuatMang(P,A);
		float Array=ViTriThucNhoNhat(P,A1);
		System.out.print("Vị trí thực nhỏ nhất là: "+ Array);
	}
}
*/

//Assignment11_9
/*Tìm vị trí của giá trị chẵn đầu tiên trong mảng 1 chiều các số nguyên. Nếu
mảng không có giá trị chẵn thì sẽ trả về -1*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static int[] NhapXuatMang(int P, int[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new int[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextInt();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static int ViTriSoNguyenChanDauTien(int P, int[] A) {
		for (int i = 0; i < P; i++)
			if (A[i] % 2 == 0)
				return i;
		return -1;
	}
	public static void main(String[] agrs) {
		int N = 0;
		int[] A=null;
		int P = NhapN(N);
		int[] A1=NhapXuatMang(P,A);
		int Array=ViTriSoNguyenChanDauTien(P,A1);
		System.out.println("Vị trí số nguyên chẵn đầu tiên là: " + Array);
		
	}

}
*/

//Assignment11_10
/*Tìm vị trí số hoàn thiện cuối cùng trong mảng 1 chiều các số nguyên. Nếu
mảng không có số hoàn thiện thì trả về giá trị -1*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static int[] NhapXuatMang(int P, int[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new int[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextInt();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static int SoHoanThien(int P) {
		int T = 0;
		for (int i = 1; i < P; i++)
			if (P % i == 0)
				T += 1;
		if (T == P)
			return 1;
		return 0;
	}

	public static int ViTriHoanThienCuoiCung(int P, int[] A) {
		for (int i = P - 1; i > 0; i--)
			if (SoHoanThien(A[i] == 1))
				return i;
		return 0;
	}

	private static boolean SoHoanThien(boolean A) {  
	  return false;
	 }
	
	public static void main(String[] agrs) {
		int N = 0;
		int[] A=null;
		int P = NhapN(N);
		int[] A1=NhapXuatMang(P,A);
		int Array=ViTriHoanThienCuoiCung(P,A1);
		System.out.println("Vị trí hoàn thiện cuối cùng: " + Array);
		
	}
}
*/

//Assignment11_11
/*Hãy tìm giá trị dương nhỏ nhất trong mảng 1 chiều các số thực. Nếu mảng
không có giá trị dương thì sẽ trả về -1*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static float[] NhapXuatMang(int P, float[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new float[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextFloat();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static float SoThucDuongNhoNhat(int P, float[] A) {
		float min = -1;
        int i;
        for(i = 0; i < A.length; i++) {
            if((min == -1 || min > A[i]) && A[i] > 0 ) {
                min = A[i];
            }
        }
        return min;
	}
	
	public static void main(String[] agrs) {
		int N = 0;
		float[] A=null;
		int P = NhapN(N);
		float[] A1=NhapXuatMang(P,A);
		float Array=SoThucDuongNhoNhat(P,A1);
		System.out.println("Giá trị dương nhỏ nhất: " + Array);
		
	}
}
*/

//Assignment11_12
/*Hãy tìm vị trí giá trị dương nhỏ nhất trong mảng 1 chiều các số thực. Nếu
mảng không có giá trị dương thì trả về -1*/
/*
package assignment_11;
import java.util.*;
public class source11 {
	
	public static int NhapN(int N) {
	Scanner Input = new Scanner(System.in);
		System.out.print("Nhập vào N độ dài của mảng A là: ");
		N = Input.nextInt();
		return N;
	}
	
	public static float[] NhapXuatMang(int P, float[] A ) {
	Scanner Input = new Scanner(System.in);
		A= new float[P];
		System.out.print("\r\n");
		for (int i=0;i<P;i++) {
			System.out.print("Nhập vào phần tử thứ "+i+": ");
			A[i] = Input.nextFloat();
		}
		
		System.out.println("Danh sách hiển thị giá trị các phần tử của mảng A là: \n");
		
		for (int i=0;i<P;i++) {
			System.out.println("A["+i+"] = "+A[i]);
		}
		return A;
	}
	
	public static int ViTriDuongNhoNhatMangSoThuc(int P, float[] A) {
		 float min = 0;
	        int pos = -1;
	        int i;
	        for(i = 0; i < A.length; i++) {
	            if((min == 0 || min > A[i]) && A[i] > 0 ) {
	            min = A[i];
	            pos = i;
	            }
	        }
	        return pos;
	}
	
	public static void main(String[] agrs) {
		int N = 0;
		float[] A=null;
		int P = NhapN(N);
		float[] A1=NhapXuatMang(P,A);
		int Array=ViTriDuongNhoNhatMangSoThuc(N,A1);
		System.out.println("Vị trí giá trị dương nhỏ nhất: " + Array);
	}
}
*/
//8/10