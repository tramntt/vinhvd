//Assignment4_1
/*Viết hàm nhập vào hai số, xuất ra số lớn nhất trong hai số*/
/*
package assignment_04;

import java.util.Scanner;

public class source4 {

	public static void main(String[] args) {
		float a,b;
		Scanner Input=new Scanner(System.in);
		
		System.out.print("Nhập vào số a : ");
		a=Input.nextFloat();
		
		System.out.print("Nhập vào số b : ");
		b=Input.nextFloat();
		
		float max=a>b?a:b;
		System.out.println("Số lớn nhất là : "+max);
	}

}
*/

//Assignment4_2
//Viết hàm nhập vào n số, xuất ra số lớn  nhất trong n số
/*
package assignment_04;

import java.util.Scanner;

public class source4 {
	public static void main(String[] args) {
		final int MIN=0;
		final int MAX=100;
		
			Scanner Input=new Scanner(System.in);
			int size;
			System.out.println("Điều kiện(0<n<100)");
			System.out.print("Nhập vào số lượng phần tử n mà bạn muốn so sánh: ");
			size=Input.nextInt();
			System.out.print("\r\n");
			if(size<MIN || size>MAX)
			{
				do
				{
					System.out.println("Xem lại điều kiện");
					System.out.print("Mời bạn nhập lại số lượng phần tử n mà bạn muốn so sánh : ");
					size=Input.nextInt();
					System.out.print("\r\n");
				}while(size<MIN || size>MAX);
			}
			
			int a[]=new int[size];
			//Hàm nhập
			for(int i=0;i<size;i++)
			{
				System.out.print("a["+i+"]=");
				a[i]=Input.nextInt();
			}
			//Hàm xuất
			System.out.println("Các giá trị bạn vừa nhập là : ");
			for(int i=0;i<size;i++)
			{
				System.out.print(a[i]);
				System.out.print("\t");
			}
			System.out.print("\r\n");
			
			//Hàm xử lí giá trị lớn nhất
			int max=a[0];
			for(int i=0;i<size;i++)
			{
				if(a[i]>max)
					max=a[i];
			}
			System.out.println("Số lớn nhất trong những giá trị bạn vừa nhập là : " + max);
		
	}
}
*/


//Assignment4_3
/*Viết hàm nhập vào ba số, xuất ra tổng lớn nhất của phép cộng từng cặp số (không
tính tường hợp cộng chính nó)*/
/*
package assignment_04;
import java.util.Scanner;
public class source4{
	public static Scanner Input=new Scanner(System.in);
	
	
	public static void main(String[] args) {
		float a,b,c;
		float max;
		System.out.print("Nhập vào số a : ");
		a=Input.nextInt();
		
		System.out.print("Nhập vào số b : ");
		b=Input.nextInt();
		
		System.out.print("Nhập vào số c : ");
		c=Input.nextInt();
		
		max=SoSanh(a,b,c);	
		System.out.println("Tổng lớn nhất của phép công từng cặp số là : "+ max);
		
	}
	public static float SoSanh(float a, float b, float c)
	{
		float ab,ac,bc,max;
		ab=a+b;
		ac=a+c;
		bc=b+c;
		max=ab;
		if(ac>max)
			max=ac;
		if(bc>max)
			max=bc;
		
		return max;
	}
}
*/









